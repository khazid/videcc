<?php
if (!function_exists('renderDataAttributes')) {
    function renderDataAttributes($attributes)
    {
        $mapped = [ ];
        foreach ($attributes as $key => $value) {
            $mapped[] = 'data-' . $key . '="' . $value . '"';
        };

        return implode(' ', $mapped);
    }
}
?>
 @if(!empty($options))
            <script type="text/javascript">
                var RecaptchaOptions = <?=json_encode($options) ?>;
            </script>
        @endif
        <script src='https://www.google.com/recaptcha/api.js?render=onload{{ (isset($lang) ? '&hl='.$lang : '') }}'></script>
        <div class="g-recaptcha" 
                style="transform:scale(0.83);-webkit-transform:scale(0.83);transform-origin:0 0;-webkit-transform-origin:0 0;" 
                data-sitekey="{{ $public_key }}" <?=renderDataAttributes($dataParams)?>></div>
        <noscript>
            
                
                        <iframe src="https://www.google.com/recaptcha/api/fallback?k={{ $public_key }}">
                        </iframe>
                
                        <textarea id="g-recaptcha-response" name="g-recaptcha-response"
                          class="g-recaptcha-response"></textarea>
       </noscript>
       <!-- @if ($errors->has('g-recaptcha-response'))
            <span class="help-block">
                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
            </span>
        @endif -->
