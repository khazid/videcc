<html lang="en">
  <body>
      <h4> Bienvenido {{$user->name}} al Sistema Drnow24</h4>
      
      <p>La informacion para tu ingreso al sistema es:
      		Usuario: {{$user->email}}
      		Clave: {{$user->password}}
      </p>

      <p>Si necesitas mas informacion por favor comunicate con nosotros a traves del correo atencion@doctornow24.com y con gusto te guiaremos con todo tu proceso.</p>

      <p>Gracias</p>

  </body>
</html>