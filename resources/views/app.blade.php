<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ env('APP_TITLE')}}</title>
    {!! Html::style('assets/css/bootstrap.css') !!}
    {!! Html::style('assets/css/bootstrap.min.css') !!}
    <!-- Morris Charts CSS 
    {!! Html::style('assets/bower_components/morrisjs/morris.css') !!}-->
    <!-- Timeline CSS 
    {!! Html::style('assets/dist/css/timeline.css') !!}-->
    <!--DRNOW css
    {!! Html::style('assets/css/drnow.css') !!}-->
    <!--Video llamda css-->
    {!! Html::style('assets/css/video.css') !!}
    {!! Html::style('assets/confirm/jquery-confirm.min.css') !!}

    <!-- DataTables CSS -->
    {!! Html::style('assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') !!}
        <!-- Datatables -->
    {!! Html::style('/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') !!}
    {!! Html::style('/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') !!}


    <!-- Fonts -->
     {!! Html::style('assets/font-awesome/css/font-awesome.min.css') !!}
   <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js does not work if you view the page via file: -->
    <!--[if lt IE 9]>
    	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
     	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    

</head>
<body>
            
	@include('partials.layout.navbar')
    @yield('content')
    @include('partials.layout.errors')
    @include('partials.layout.messages')
    
    <!-- Scripts -->

    {!! Html::script('assets/js/jquery-2.1.4.min.js') !!}
    {!! Html::script('assets/js/bootstrap.min.js') !!}
    <!--{!! Html::script('assets/js/map.js') !!}-->
    {!! Html::script('assets/js/validador.js') !!}
    {!! Html::script('assets/confirm/jquery-confirm.min.js') !!}
    {!! Html::script('assets/js/ion.sound.js') !!}
    {!! Html::script('assets/js/ion.sound.min.js') !!}
    @stack('scripts')

   <!-- DataTables JavaScript -->
    {!! Html::script('assets/bower_components/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! Html::script('assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') !!}
    <!-- Datatables -->
    {!! Html::script('/vendors/datatables.net/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/dataTables.buttons.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.flash.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.html5.min.js') !!}
    {!! Html::script('/vendors/datatables.net-buttons/js/buttons.print.min.js') !!}
    {!! Html::script('/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') !!}
    {!! Html::script('/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') !!}
    {!! Html::script('/vendors/datatables.net-scroller/js/dataTables.scroller.min.js') !!}
    {!! Html::script('/vendors/jszip/dist/jszip.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/pdfmake.min.js') !!}
    {!! Html::script('/vendors/pdfmake/build/vfs_fonts.js') !!}

        <!-- bootstrap-daterangepicker -->
    {!! Html::script('/vendors/moment/moment.min.js') !!}
    {!! Html::script('/vendors/datepicker/daterangepicker.js') !!}

    <!-- script validacion aseguradora -->
<script type="text/javascript">
    $(document).ready(function($) {
        $('#insurance_id').change(function(){
            if($( this ).val()=='1'){
                $('.policy').hide();
                $('.policy_number').removeAttr('required');
            }else{
                $('.policy').show();
                $('.policy_number').attr('required','required');
            }
        });
    });
</script>

    @yield('script')
     
</body>
<footer>
         <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; KeyAllCall Kall - 2017</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                
                            </span></a>
                        </li>
                        <li><a href="#"><span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                
                            </span></a>
                        </li>
                        <li><a href="#"><span class="fa-stack">
                                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
                                
                            </span></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="#">Legales</a>
                        </li>
                        <li><a href="#">Terminos y condiciones</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</html>