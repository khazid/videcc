@extends('app')

@section('content')
<div class="container">
    <div class="page-header">
        <h1>Detalle del Historial</h1>
    </div>
    <div class="row">
        <div class="all" style="display: inline;">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="panel panel-default">
                        <div class="panel-heading">Detalle del Historial</div>                    
                        <div class="panel-body">
                            <br>
                            {!! Form::open(['class' => 'form']) !!}
                            <table class="table table-condensed table-striped table-bordered">                                
                                <tbody>
                                	<div class="form-group">
                                        <label>Fecha de tratamiento</label>
                                        {!! Form::input('text', 'created', date_format( $history->created_at, 'd/m/Y H:i:s'), ['class'=> 'form-control']) !!}                            
                                    </div>
                                    <div class="form-group">
                                        <label>Motivo de la Consulta</label>
                                        {!! Form::input('text', 'reason', $history->reason, ['class'=> 'form-control']) !!}                            
                                    </div>
                                    <div class="form-group">
                                        <label>Diagnostico</label>
                                        {!! Form::input('text', 'current_illness', $history->current_illness, ['class'=> 'form-control']) !!}                            
                                    </div>
                                    <div class="form-group">
                                        <label>Tratamiento a seguir:</label>
                                        {!! Form::input('text', 'treatment', $history->treatment, ['class'=> 'form-control']) !!}                            
                                    </div>  
                                    <div class="form-group">
                                        <label>Sugerencias y recomendaciones:</label>
                                        {!! Form::input('text', 'suggestion', $history->suggestion, ['class'=> 'form-control']) !!}                            
                                    </div> 
                                    <div class="form-group">
                                        <label>Indicaciones:</label>
                                        {!! Form::input('text', 'indications', $history->indications, ['class'=> 'form-control']) !!}                            
                                    </div> 
                                    <div class="form-group">
                                        <label>Examenes complementarios:</label>
                                        {!! Form::input('text', 'exam', $history->exam, ['class'=> 'form-control']) !!}                            
                                    </div>      
                                    <div class="form-group">
                                        <label>Receta medica</label>
                                        {!! Form::input('text', 'prescription', $history->prescription, ['class'=> 'form-control']) !!}                            
                                    </div>      
                                    <div class="form-group">
                                        <label>Clinica a Referir:</label>
                                        {!! Form::input('text', 'doctor_reference', $history->doctor_reference, ['class'=> 'form-control']) !!}                            
                                    </div> 
                                    <div class="form-group">
                                        <label>Motivo de la referencia:</label>
                                        {!! Form::input('text', 'reference_reason', $history->reference_reason, ['class'=> 'form-control']) !!}                            
                                    </div>                                                  
                                                                                    
                                </tbody>
                            </table>    

                    <div>
                        <a href="{{ URL::previous() }}" class="btn btn-success">Volver</a>                      
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection