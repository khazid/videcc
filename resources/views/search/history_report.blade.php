@extends('app')
@section('content')
<div class="container">
  <div class="page-header">
      <div class="page-header">
      <div align="center"><img src="{{ asset('images/img/asegurado.png') }}" class="img-responsive" alt="">
  <h1>Historia del Asegurado</h1>
  </div>

<div class="row">
        <div class="all" style="display: inline;">
            <div class="row">
                <div class="col-md-12 ">
                    <div class="panel panel-default">
                        <div class="panel-heading"></div>

                        <div class="panel-body">                 
                            {!! Form::open(['novalidate', 'class' => 'form-inline']) !!}
                            <div class="form-group">
                            <label for="name">Asegurado</label>
                               {!! Form::input('text', 'name', $per->first_name . ' ' . $per->last_name , ['class'=> 'form-control']) !!}
                            </div>
                             <div class="form-group">
                            <label for="identity_number">Código de Póliosa</label>
                                {!! Form::input('text', 'name', $per->policy_number , ['class'=> 'form-control'] ) !!}
                            </div>
                             <div class="form-group">
                            
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div> 


    <div class="row">
               <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="{{ URL::previous() }}" class="btn btn-success">Volver</a>
                            <a href="{{ route('reportinsured',['id' => $per->id] ) }}" class="btn btn-primary">Todos</a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                         <tr>
                                            <th></th>
                                            <th>Fecha de ingreso</th>
                                            <th>Motivo del ingreso</th>
                                            <th>Antecedentes</th>
                                            <th>Clinca a Referir</th>
                                            <th>{{ trans('nav.action') }}</th  
                                          </tr>
                                    </thead>
                                   <tbody>
                                        @foreach($info as $i)
                                        <tr>
                                            <td>{{ $i->id }}</td>
                                            <td>{{date_format( $i->created_at, 'd/m/Y H:i:s') }}</td>
                                            <td>{{ $i->reason }}</td>
                                            <td>{{ $i->background }}</td>
                                            <td>{{ $i->doctor_reference }}</td>
                                            <td>
                                             <a class="glyphicon glyphicon-eye-open" title='Ver Detalle.'href="{{ route('history/detail',['id' => $i->id] )}}" > </a>
                                            </td>
                                           
                                        </tr>
                                        @endforeach
                                    </tbody> 
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
   
  </div> <!-- /. ROW-->
</div>
{!! Html::script('assets/bower_components/jquery/dist/jquery.min.js') !!}

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
@endsection