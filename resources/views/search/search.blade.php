@extends('app')
@section('content')
<div class="container">
  <div class="page-header">
      <div class="page-header">
      <div align="center"><img src="{{ asset('images/img/asegurado.png') }}" class="img-responsive" alt="">
  <h1>Busqueda de Asegurado Registrado</h1>
  </div>
    <div class="row">
               <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            
                              <a href="{{ route('search.index') }}" class="btn btn-primary">All</a>
                            
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                         <tr>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Cedula</th>
                                            <th>Nro de poliza</th>
                                            <th>{{ trans('nav.action') }}</th>  

                                          </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($info as $i)
                                        <tr>
                                            <td>{{ $i->first_name }}</td>
                                            <td>{{ $i->last_name }}</td>
                                            <td>{{ $i->identity_number }}</td>
                                            <td>{{ $i->policy_number }}</td>
                                            <td>
                                             <a class="glyphicon glyphicon-eye-open" title='Ver Historial'href="{{ route('reportinsured',['id' => $i->id] )}}" > </a>
                                            </td>
                                           
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
   
  </div> <!-- /. ROW-->
</div>
{!! Html::script('assets/bower_components/jquery/dist/jquery.min.js') !!}

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
@endsection