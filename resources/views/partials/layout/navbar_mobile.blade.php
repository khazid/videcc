    @inject('options','auth')
    <nav class="navbar navbar-default navbar-fixed-top navbar-shrink" style="padding: 0px;">
        <div class="container-fluid">
		    <div class="navbar-header">
		        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			    <span class="sr-only">Toggle Navigation</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			</button>

				<a class="navbar-brand page-scroll" href="{{URL::to('/')}}">
				  <img src="{{ asset('images/img/logo_drnow24_pq.png') }}" alt="logo_header" style="width:160px;height: 33px;"></a>
			<!--<p class="navbar-brand">{{ env('APP_TITLE') ? : "Laravel" }}</p>-->
		    </div>
		    
		</div>
	</nav>


