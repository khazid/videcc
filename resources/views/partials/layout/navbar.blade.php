    @inject('options','auth')
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
		    <div class="navbar-header">
		        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			    <span class="sr-only">Toggle Navigation</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			</button>

				<a class="navbar-brand page-scroll" href="{{URL::to('/')}}">
				 
				  <span class="fa-stack fa-lg">
                                <i class="fa fa-circle fa-stack-2x text-primary"></i>
                                <i class="fa fa-phone fa-stack-1x fa-inverse"></i>
                                
                  </span>
				  </a>
			
		    </div>
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
				    
				    @if (Auth::guest())
				        <!--<li><a href="{{route('auth/login')}}">{{ trans('nav.login') }}</a></li>	-->					
				    @else
				    	<li><a href="{{route('home/index')}}">{{ trans('nav.home') }}</a></li>
				     	
				     	@foreach($options->options() as $opt) 
				     		
		               		<li role="presentation" class="dropdown">
	                                 <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
	                                 {{$opt->menuname}} <span class="caret"></span></a>
	                                 	<ul class="dropdown-menu">
	                                 		@foreach(Auth::permission() as $p)
	                                 		
				                          		@if($p->menu_id === $opt->idmenu)         	

				                            		<li><a href="{!! ($p->slug=='#' ? '#' : route($p->slug)) !!}">{{ $p->permname}}</a></li>	
 
				                                @endif
				                         	@endforeach           
				                        </ul>
				                         

                         	</li>  
		                 @endforeach
		                 
		                <li role="presentation" class="dropdown">
                                 <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                 {{ Auth::user()->name }} <span class="caret"></span></a>
	                            <ul class="dropdown-menu">
	                                 <li><a href="{{route('personal/reset')}}">Cambiar contraseña</a></li>
	                                 <li><a href="{{route('auth/logout')}}">{{ trans('nav.logout') }}</a></li>
	                            </ul>
                         </li> 
		                
			        @endif
				</ul>
			</div>
		</div>
	</nav>


