<div class="container errors-container">
    @if (Session::has('messages'))
    	@foreach (Session::get('messages') as $type => $message)
		    <div class="alert alert-{{ $type }} alert-dismissible" role="alert">
		    	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<span class="message">{{ $message }}</span>
		    </div>
	    @endforeach
	@endif	
</div>