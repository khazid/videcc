@extends('app')
@section('content')
<div class="container">
<div class="page-header">
        <h1>Cambio de contraseña</h1>
    </div>
    <div class="rowmargin">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                
                 <div class="panel-body">
                    {!! Form::open(['route' => 'personal/update', 'class' => 'form']) !!}
                       
                        <!--<div class="form-group">
                            <label>{{ trans('form.label.email') }}</label>
                            {!! Form::email('email', null, ['class'=> 'form-control']) !!}
                        </div>-->
                        <div class="form-group">
                            <label>Nueva Contraseña</label>
                            {!! Form::password('password', ['class'=> 'form-control']) !!}
                        </div>
                        <!--<div class="form-group">
                            <label>{{ trans('form.label.password_confirmation') }}</label>
                            {!! Form::password('password_confirmation', ['class'=> 'form-control']) !!}
                        </div>
                        <a href="{{ URL::previous() }}" class="btn btn-xl" role="button">Volver</a>-->
                        {!! Form::submit('Enviar', ['class' => 'btn btn-xl']) !!}
                        
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

