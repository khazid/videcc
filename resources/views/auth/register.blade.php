@extends('app')
 
@section('content')
<div class="container">
<div class="page-header">
  <h1>Registro de Usuario Asegurado</h1>
  </div>
    <div class="row">
    <div class="all" style="display: inline;">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('form.signup.title') }}</div>

                <div class="panel-body">
                    {!! Form::open(['route' => 'auth/register', 'class' => 'form']) !!}
                         @foreach($insured as $insured)

                            
                            <div class="form-group">
                                <label>{{ trans('form.label.name') }}</label>
                                {!! Form::input('text', 'name', $insured->first_name, ['class'=> 'form-control']) !!}                            
                            </div>
                            <div class="form-group">
                                <label>{{ trans('form.label.last_name') }}</label>
                                {!! Form::input('text', 'last_name', $insured->last_name, ['class'=> 'form-control']) !!}                            
                            </div>
                            <div class="form-group">
                                <label>{{ trans('form.label.cedula') }}</label>
                                {!! Form::input('text', 'cedula', $insured->identity_number, ['class'=> 'form-control']) !!}                            
                            </div>
                            <div class="form-group">
                                <label>{{ trans('form.label.cod_pol') }}</label>
                                {!! Form::input('text', 'cod_pol', $insured->policy_number, ['class'=> 'form-control']) !!}                            
                            </div>
                            <div class="form-group">
                                <label>{{ trans('form.label.email') }}</label>
                                {!! Form::email('email', '', ['class'=> 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label>{{ trans('form.label.password') }}</label>
                                {!! Form::password('password', ['class'=> 'form-control']) !!}
                            </div>

                            <div class="form-group">
                                <label>{{ trans('form.label.password_confirmation') }}</label>
                                {!! Form::password('password_confirmation', ['class'=> 'form-control']) !!}
                            </div>
                            
                            <div>                            
                                <a href="{{ URL::previous() }}" class="btn btn-xl">Volver</a>                             
                                {!! Form::submit(trans('form.signup.submit'),['class' => 'btn btn-xl']) !!}
                       
                            </div>                           
                        @endforeach 
                       
                         <?php
                            if ($insured->first() == NULL) {

                               echo'<div class="alert alert-warning alert-dismissable">
                                          <button  type="button" class="close" data-dismiss="alert">&times;</button>
                                          <strong>¡Disculpe!</strong> Número de póliza invalido.                                          
                                     </div>';  
                                 
                             }
                          ?>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection