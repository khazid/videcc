@extends('app')
@section('content')
    <div class="container">
        <div class="rowmargin">
            <div align="center"><img src="{{ asset('images/img/logo_drnow24.png') }}" class="img-responsive" alt=""></div>
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">{{ trans('form.login.title') }}</div>
                    <div class="panel-body">
                        {!! Form::open(['route' => 'auth/login', 'class' => 'form']) !!}
                            <div class="form-group">
                                <label>{{ trans('form.label.email') }}</label>
                                {!! Form::email('email', '', ['class'=> 'form-control']) !!}
                            </div>
                            <div class="form-group">
                                <label>{{ trans('form.label.password') }}</label>
                                {!! Form::password('password', ['class'=> 'form-control', 'required' => 'required' ]) !!}
                            </div>
                            
                           
                             {!! Recaptcha::render() !!} 
                            
                            <!--<div class="checkbox"> 
                                <label><input name="remember" type="checkbox">{{ trans('form.label.remember') }}</label>
                            </div> -->                              
                            <div class="form-group" align="center">
                                
                                <button type="submit" class="btn btn-xl"><i class="fa fa-sign-in" aria-hidden="true"></i> Ingresar </button>
                                </br>
                                <a href="{{ url('password/email') }}">{{ trans('passwords.forgot') }}</a> 
                                <a href="{{ url('activationstaff') }}">{{ trans('nav.signup_adm') }}</a>
                                                           
                            </div>
                            
                        {!! Form::close() !!}
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsection