@extends('app')
<title>Kall | Called</title>
@section('content')
{!! Html::style('assets/css/map.css') !!}

    {!! Html::script('assets/audio-recorder/MediaStreamRecorder.js') !!}
	{!! Html::script('assets/audio-recorder/gumadapter.js') !!}
    {!! Html::script('assets/sipml5/SIPml-api.js?svn=250') !!}
    {!! Html::script('assets/sipml5/callcenter2.js') !!}
    {!! Html::script('assets/sipml5/callcenter.js') !!}

    <!-- Styles -->

    <!-- <link href="./assets/css/bootstrap-responsive.css" rel="stylesheet" /> !!-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="./assets/ico/favicon.ico" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="./assets/ico/apple-touch-icon-114-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="./assets/ico/apple-touch-icon-72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" href="./assets/ico/apple-touch-icon-57-precomposed.png" />

	
<style type="text/css">
    
    label{
        color: #2c4859;
    }

    h4{
        color: white;
    }
</style>
    <script type="text/javascript">
         // sends SIP REGISTER request to login
         
        function sipRegister() {
            // catch exception for IE (DOM not ready)
            try {
                btnRegister.disabled = true;
                if (!txtRealm.value || !txtPrivateIdentity.value || !txtPublicIdentity.value) {
                    txtRegStatus.innerHTML = '<b>Please fill madatory fields (*)</b>';
                    btnRegister.disabled = false;
                    return;
                }
                var o_impu = tsip_uri.prototype.Parse(txtPublicIdentity.value);
                if (!o_impu || !o_impu.s_user_name || !o_impu.s_host) {
                    txtRegStatus.innerHTML = "<b>[" + txtPublicIdentity.value + "] is not a valid Public identity</b>";
                    btnRegister.disabled = false;
                    return;
                }

                // enable notifications if not already done
                if (window.webkitNotifications && window.webkitNotifications.checkPermission() != 0) {
                    window.webkitNotifications.requestPermission();
                }

                // save credentials
                ///saveCredentials();

                $('#callcenter').val("true");//se coloca la bandera de callcenter en true para reloguear al hacer refresh
                //$('#available').val("false");//se coloca al medico como No Disponible para videollamada
                $('#buscadorasegurado').show();
                //$('#container').hide();
                

                // update debug level to be sure new values will be used if the user haven't updated the page
                SIPml.setDebugLevel((window.localStorage && window.localStorage.getItem('org.doubango.expert.disable_debug') == "true") ? "error" : "info");

 
              // create SIP stack
              
                oSipStack = new SIPml.Stack({
                    realm: txtRealm.value,
                    impi: txtPrivateIdentity.value,
                    impu: txtPublicIdentity.value,
                    password: txtPassword.value,
                    display_name: txtDisplayName.value,
                    websocket_proxy_url: '{{$sata->websocket_url}}', //(window.localStorage ? window.localStorage.getItem('org.doubango.expert.websocket_server_url') : null),
                    outbound_proxy_url: '{{$sata->sipoutbound_url}}', //(window.localStorage ? window.localStorage.getItem('org.doubango.expert.sip_outboundproxy_url') : null),
                    ice_servers: (_ice_servers='{{$sata->ice_servers}}' ? _ice_servers : null), //(window.localStorage ? window.localStorage.getItem('org.doubango.expert.ice_servers') : null),
                    enable_rtcweb_breaker: ('{{$sata->rtcweb_breaker}}'=='1' ? true : false), //(window.localStorage ? window.localStorage.getItem('org.doubango.expert.enable_rtcweb_breaker') == "true" : false),
                    events_listener: { events: '*', listener: onSipEventStack },
                    enable_early_ims: ('{{$sata->early_ims_3gpp}}'=='1' ? true : true), //(window.localStorage ? window.localStorage.getItem('org.doubango.expert.disable_early_ims') != "true" : true), // Must be true unless you're using a real IMS network
                    enable_media_stream_cache: ('{{$sata->en_cache}}'=='1' ? true : false), //(window.localStorage ? window.localStorage.getItem('org.doubango.expert.enable_media_caching') == "true" : false),
                    bandwidth: (_bandwidth='{{$sata->max_bandwidth}}' ? tsk_string_to_object(bandwidth) : null), //(window.localStorage ? tsk_string_to_object(window.localStorage.getItem('org.doubango.expert.bandwidth')) : null), // could be redefined a session-level
                    video_size: (_video_size='{{$sata->video_size}}' ? tsk_string_to_object(_video_size) : null), //(window.localStorage ? tsk_string_to_object(window.localStorage.getItem('org.doubango.expert.video_size')) : null), // could be redefined a session-level
                    sip_headers: [
                            { name: 'User-Agent', value: 'IM-client/OMA1.0 sipML5-v1.2016.03.04' },
                            { name: 'Organization', value: 'Doubango Telecom' }
                    ]
                }
                );
               
                if (oSipStack.start() != 0) {
                    txtRegStatus.innerHTML = '<b>Failed to start the SIP stack</b>';
                }
                else return;
            }
            catch (e) {
                txtRegStatus.innerHTML = "<b>2:" + e + "</b>";
            }
            btnRegister.disabled = false;
        }
        
        
        var remoteSIPAudio;
        var localSIPAudio;
        var timeInterval = 3600 * 1000;
            //var ifcallended=false;
				//console.log(specificName);
            function mediaRecorder(stream) {
					
                varobj = new MediaStreamRecorder(stream);
                varobj.stream = stream;

					varobj.recorderType = MediaRecorderWrapper; 
					 //StereoAudioRecorder; 
					 //MediaRecorderWrapper;

                varobj.audioChannels = 2;
					 varobj.specificName='SIP';
					 //console.log(specificName);
					 //remoteSipAudio.start(timeInterval);
					return varobj;
            }
    </script>
    
<!-- ------------------------ VIDEOLLAMADA INFORMACION PACIENTE ------------------------------ -->

<div class="container">
<div class="rowmargin">
<div class="col-sm-12 ">
<div class="col-lg-12 text-center">
   <h2 class="section-heading">Answer the call !!</h2>
                        
</div>
@if (Session::has('message'))
       
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span class="message">{{ Session::get('message') }}</span>
            </div>
  @endif
  {!! Form::open(['route' => 'video.store', 'method' => 'post', 'validate','id'=>'formulario']) !!}
  <input type="hidden" name="sound" id="sound" value="{!!env('APP_SOUND_DOC') !!}">
  <input type="hidden" name="available" id="available" value="true">
  <input type="hidden" name="busy" id="busy" value="false">
  <input type="hidden" name="callcenter" id="callcenter" value="{{$logcall}}">
  <input type="hidden" name="callcenterid" id="callcenterid">
  <input type="hidden" name="videocallid" id="videocallid">
   <input type="hidden" name="_token" value="{{csrf_token()}}" id="token"> 
   <input type="hidden" name="userId" value="{{$id_user}}" id="userId">
   <input type="hidden" name="callerfile"  id="callerfile">
   <input type="hidden" name="calledfile"  id="calledfile">

 <div class="col-sm-4" style=" border-radius: 50px 5px 50px 5px; background: rgba(153, 153, 153, 0.54);padding: 20px; margin-right: 5px; height:620px; width: 350px;" >
                        <div style="padding-top: 20px;">
                             <h4>Your Info: </h4> 
                                <label>Name:  Jhon Doe</label><br/>
                                <label>Email: jhondoe@xxx.com</label>
                             <h4>Caller Info: </h4> 
                                <label>Name:  Christian Blade</label><br/>
                                <label>Email: chisrblade@xxx.com</label>
                        </div>
                        <div class="col-lg-12 text-center">
                               
                            <!-- ------------------------- CONTADOR ------------------------- -->
                              <div id="contenedorreloj" style="display:none;">
                                <div class="reloj" id="Horas">00</div>
                                <div class="reloj" id="Minutos">:00</div>
                                <div class="reloj" id="Segundos">:00</div>
                                <div class="reloj" id="Centesimas" style="display:none;">:00</div>
                            
                              </div>
                        
                        </div>
        <div class="col-lg-12" style="padding-top: 10px">
   
           
                <input type="hidden" style="width: 100%; height: 100%" id="txtDisplayName" value="{{$ippbxdata->name}}" placeholder="e.g. John Doe" />
                <input type="hidden" style="width: 100%; height: 100%" id="txtPrivateIdentity" value="{{$ippbxdata->priv_identity}}" placeholder="e.g. +33600000000" />
                <input type="hidden" style="width: 100%; height: 100%" id="txtPublicIdentity" value="{{$ippbxdata->pub_identity}}"placeholder="e.g. sip:+33600000000@doubango.org" />
                <input type="hidden" style="width: 100%; height: 100%" id="txtRealm" value="{{$ippbxdata->realm}}" placeholder="e.g. doubango.org" />
                <input type="hidden" style="width: 100%; height: 100%" id="txtPassword" value="{{$ippbxdata->password}}" />
                <label style="width: 100%;" align="center" id="txtRegStatus"></label>
                
                <h4> Telephone Switchgear </h4>
                <input type="button" class="btn btn-warning" id="btnRegister" value="LogIn" disabled onclick='sipRegister();' />
                            &nbsp;
                <input type="button" class="btn btn-danger" id="btnUnRegister" value="LogOut" disabled onclick='sipUnRegister();' />
            
           <br />
            <div id="divCallCtrl" >
                <label style="width: 100%;" align="center" id="txtCallStatus">
                </label>
                <h4> Telephone Call Controll </h4>
              <!--  <br /> !!-->
                <table style='width: 100%;'>
                    <tr>
                        <td style="white-space:nowrap;">
                            <input type="text" style="width: 100%; height:100%; display: none" id="txtPhoneNumber" value="" placeholder="Enter phone number to call" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="1" align="left">
                            <div class="btn-toolbar" style="margin: 0; vertical-align:middle">
                                <!--div class="btn-group">
                                    <input type="button" id="btnBFCP" style="margin: 0; vertical-align:middle; height: 100%;" class="btn btn-primary" value="BFCP" onclick='sipShareScreen();' disabled />
                                </div-->
                                <div id="divBtnCallGroup" class="btn-group">
                                    <button id="btnCall" disabled class="btn btn-primary" data-toggle="dropdown">Call</button>
                                </div>
                             <!-- &nbsp;&nbsp; !!-->
                                <div class="btn-group">
                                    <input type="button" id="btnHangUp" style="margin: 0; vertical-align:middle; height: 100%;" class="btn btn-primary" value="Hangout" onclick='sipHangUp();' disabled />
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td id="tdVideo" class='tab-video' style="display: none">
                            <div id="divVideo" class='div-video'>
                                <div id="divVideoRemote" style='position:relative; border:1px solid #009; height:100%; width:100%; z-index: auto; opacity: 1'>
                                    <video class="video" width="100%" height="100%" id="video_remote" autoplay="autoplay" style="opacity: 0;
                                        background-color: #000000; -webkit-transition-property: opacity; -webkit-transition-duration: 2s;"></video>
                                </div>

                                <div id="divVideoLocalWrapper" style="margin-left: 0px; border:0px solid #009; z-index: 1000">
                                    <iframe class="previewvideo" style="border:0px solid #009; z-index: 1000"> </iframe>
                                    <div id="divVideoLocal" class="previewvideo" style=' border:0px solid #009; z-index: 1000'>
                                        <video class="video" width="100%" height="100%" id="video_local" autoplay="autoplay" muted="true" style="opacity: 0;
                                            background-color: #000000; -webkit-transition-property: opacity;
                                            -webkit-transition-duration: 2s;"></video>
                                    </div>
                                </div>
                                <div id="divScreencastLocalWrapper" style="margin-left: 90px; border:0px solid #009; z-index: 1000">
                                    <iframe class="previewvideo" style="border:0px solid #009; z-index: 1000"> </iframe>
                                    <div id="divScreencastLocal" class="previewvideo" style=' border:0px solid #009; z-index: 1000'>
                                    </div>
                                </div>
                                <!--div id="div1" style="margin-left: 300px; border:0px solid #009; z-index: 1000">
                                    <iframe class="previewvideo" style="border:0px solid #009; z-index: 1000"> </iframe>
                                    <div id="div2" class="previewvideo" style='border:0px solid #009; z-index: 1000'>
                                      <input type="button" class="btn" style="" id="Button1" value="Button1" /> &nbsp;
                                      <input type="button" class="btn" style="" id="Button2" value="Button2" /> &nbsp;
                                    </div>
                                </div-->
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align='center'>
                            <div id='divCallOptions' class='call-options' style='opacity: 0; margin-top: 0px'>
                                <input type="button" class="btn" style="" id="btnFullScreen" value="FullScreen" disabled onclick='toggleFullScreen();' /> &nbsp;
                                <input type="button" class="btn" style="" id="btnMute" value="Mute" onclick='sipToggleMute();' /> &nbsp;
                                <input type="button" class="btn" style="" id="btnHoldResume" value="Hold" onclick='sipToggleHoldResume();' /> &nbsp;
                                <input type="button" class="btn" style="" id="btnTransfer" value="Transfer" onclick='sipTransfer();' /> &nbsp;
                                <input type="button" class="btn" style="" id="btnKeyPad" value="KeyPad" onclick='openKeyPad();' />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <!-- <div class="span4 well">
                <span>En caso de error en la llamada durante la consulta presione el boton "Error En llamada", para indicar que la llamada no fue finalizada de forma correcta</span>
                <a href="#" class="btn btn-danger" id="callerror">Error en llamada</a>
            </div> -->
       
        <object id="fakePluginInstance" classid="clsid:69E4A9D1-824C-40DA-9680-C7424A27B6A0" style="visibility:hidden;height: 40px;" > </object>
        <div id='divGlassPanel' class='glass-panel' style='visibility:hidden'></div>
    <!-- KeyPad Div -->
    <div id='divKeyPad' class='span2 well div-keypad' style="left:0px; top:0px; width:250; height:240; visibility:hidden">
        <table style="width: 100%; height: 100%">
            <tr><td><input type="button" style="width: 33%" class="btn" value="1" onclick="sipSendDTMF('1');" /><input type="button" style="width: 33%" class="btn" value="2" onclick="sipSendDTMF('2');" /><input type="button" style="width: 33%" class="btn" value="3" onclick="sipSendDTMF('3');" /></td></tr>
            <tr><td><input type="button" style="width: 33%" class="btn" value="4" onclick="sipSendDTMF('4');" /><input type="button" style="width: 33%" class="btn" value="5" onclick="sipSendDTMF('5');" /><input type="button" style="width: 33%" class="btn" value="6" onclick="sipSendDTMF('6');" /></td></tr>
            <tr><td><input type="button" style="width: 33%" class="btn" value="7" onclick="sipSendDTMF('7');" /><input type="button" style="width: 33%" class="btn" value="8" onclick="sipSendDTMF('8');" /><input type="button" style="width: 33%" class="btn" value="9" onclick="sipSendDTMF('9');" /></td></tr>
            <tr><td><input type="button" style="width: 33%" class="btn" value="*" onclick="sipSendDTMF('*');" /><input type="button" style="width: 33%" class="btn" value="0" onclick="sipSendDTMF('0');" /><input type="button" style="width: 33%" class="btn" value="#" onclick="sipSendDTMF('#');" /></td></tr>
            <tr><td colspan=3><input type="button" style="width: 100%" class="btn btn-medium btn-danger" value="close" onclick="closeKeyPad();" /></td></tr>
        </table>
    </div>
    <!-- Call button options -->
    <ul id="ulCallOptions" class="dropdown-menu" style="visibility:hidden">
        <li><a href="#" onclick='sipCall("call-audio");'>Audio</a></li>
        <li><a href="#" onclick='sipCall("call-audiovideo");'>Video</a></li>
        <li id='liScreenShare'><a href="#" onclick='sipShareScreen();'>Screen Share</a></li>
        <li class="divider"></li>
        <li><a href="#" onclick='uiDisableCallOptions();'><b>Disable these options</b></a></li>
    </ul>
     <!-- Audios -->
    <audio id="audio_remote" autoplay="autoplay"> </audio>
    <audio id="ringtone" loop src="assets/sipml5/sounds/ringtone.wav"> </audio>
    <audio id="ringbacktone" loop src="assets/sipml5/sounds/ringbacktone.wav"> </audio>
    <audio id="dtmfTone" src="assets/sipml5/sounds/dtmf.wav"> </audio>
    
    </div>
<!------ BOTON DE SALIR ---->
    <div align="center">
        <a href="{{URL::to('/')}}" class="btn btn-success" id="leave">Leave</a>
    </div>
        

    </div> <!-- col-md-4-->

<!--------- VIDEO ---------->
        <div class="col-sm-8" style=" border-radius: 5px 50px 5px 50px; background: rgba(153, 153, 153, 0.54);padding: 10px; margin-right: 5px; height:620px;" >
              
              <div class="col-sm-12 col-lg-12" align="center">
                <div id='videos'>
                    <div id="container">
                      <div id="big_video">
                        <video id='remoteVideo' class="remoteVideoIns"  autoplay ></video>
                      </div>
                      <div id="small_video">
                            <video id='localVideo' class="localVideoIns" autoplay muted></video>
                          </div>
             
                    </div>
                  </div>
              </div> 
          </div>   
          </div>
        </div>  

</div> <!-- /. container- -->

@endsection

<!-- ------------------------ SCRIPTS ------------------------------ -->
@push('scripts')

<script type="text/javascript">
        try {
            var pageTracker = _gat._getTracker("UA-6868621-19");
            pageTracker._trackPageview();
        } catch (err) { }
        
          //-------------------------------------------------
  
    </script>
<!-- {!! Html::script('assets/js/videodoc.js') !!} --> <!--para pruebas locales de videollamada -->


{!! Html::script('assets/js/cronometro.js') !!}


<script src="https://www.doctornow24.com:2905/socket.io/socket.io.js"></script>

    <!-- WEB RTC Code -->
    {!! Html::script('assets/js/webrtc.adapter.js') !!}
    {!! Html::script('assets/js/conference_doc.js') !!} <!-- configuracion de la videollamada video y audio-->
    {!! Html::script('assets/js/video.js') !!}    <!-- configuracion de la videollamada responder-->


 @endpush     
