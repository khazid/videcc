@extends('app')

@section('content')

<div class="container">

    
    <div class="rowmargin">
   
    <div class="col-sm-12 ">
            <div class="col-sm-4 col-md-4 col-xs-4" style=" border-radius: 50px 5px 50px 5px; background: rgba(153, 153, 153, 0.54);padding: 20px; margin-right: 5px; height:600px; width: 750px;" >
                     <div style="padding-top: 20px;padding-right: 15px;padding-left: 15px;">
                         <h1>Hello! Make a quick test, you will love it</h1><br/><br/><br/><br/>
                      <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p><br/><br/><br/>
                      <p><a class="btn btn-info btn-lg" href="#" role="button">Learn more</a></p>
                     </div>
                      
                     
            </div> <!-- col-md-4-->

            <div class="col-sm-4 col-md-4 col-xs-4" style=" border-radius: 5px 50px 5px 50px; background: rgba(153, 153, 153, 0.54);padding: 20px; margin-right: 10px;height:600px; width: 350px;" >
                        <div class="col-lg-12 text-center">
                             
                            <h2 class="section-heading">Make a Call !!</h2>
                         
                        </div>
                <div class="panel-body">
                  <div class="col-lg-12 " style="padding-top: 40px">  
                    {!! Form::open(['route' => 'validate-insured', 'method' => 'post', 'class' => 'form ']) !!}
                       
                                    <div class="form-group">
                                        <label for="insurance">Access:</label>  
                                        {!! Form::select('accestype', $data,null,['class' => 'form-control','id'=>'accestype']) !!}                                                            
                                    </div>
                                    <div class="form-group">
                                        <label for="cedula">Name:</label>
                                        <input type="text"  required="required" class="form-control" name="name" >                          
                                    </div>
                                    <div class="form-group">
                                        <label for="cedula">Email:</label>
                                        <input type="text"  required="required" class="form-control" name="email" >                          
                                    </div>
                                     
                                   
                                    <!-- Integracion Captcha -->
                                    {!! Recaptcha::render() !!}
                                    
                                       
                                    <div class="col-lg-12 text-center" style="padding-top: 40px;">

                                        <div id="success"></div>
                                        <!--  <a href="{{ URL::previous() }}" class="btn btn-success">Volver</a> -->
                                        <button type="submit" class="btn btn-primary">Enter</button>
                                    </div>                                
                                  
                    </div>
                </div>

                {!! Form::close() !!}
            </div> <!-- col-md-4-->
        </div>    
    </div> <!-- /.rowmargin-->
</div><!-- /.container-->

@endsection

