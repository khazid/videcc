@extends('app')

<title>Kall | Caller</title>
<style type="text/css">
    
    label{
        color: #2c4859;
    }

    h4{
        color: white;
    }
</style>
@section('content')

<div class="container">
    
<div class="rowmargin">
<div class="col-sm-12 ">
<div class="col-lg-12 text-center">
    <h2 class="section-heading">Begin your call !!</h2>
</div>
            <div class="col-sm-4" style=" border-radius: 50px 5px 50px 5px; background: rgba(153, 153, 153, 0.54);padding: 20px; margin-right: 5px; height:620px; width: 350px;" >
            <input type="hidden" name="sound" id="sound" value="{!!env('APP_SOUND_PAC') !!}">
             
             <div style="padding-top: 20px;">
                             <h4>Your Info: </h4> 
                                <label>Name:  Jhon Doe</label><br/>
                                <label>Email: jhondoe@xxx.com</label>
             </div>           
              <div class="col-lg-12" style="padding-top: 40px">
                  
                <div class="col-lg-12 text-center" style="font-size: 10px">
                 
                  <div class="col-lg-12" style="padding-top: 40px">
                    <a href="{{URL::to('/')}}" class="btn btn-success" id="leave">Leave</a>
                    <button id="call" class="btn btn-primary" >Call</button>
                    <button id="endcall" style ="display:none" class="btn btn-danger" >End Call</button>
                    <h4 id="waitlabel" style ="display:none;" class="section-subheading text-muted">En breves momentos su llamada sera atendida por favor espere...<i id="wait" class="fa fa-spinner fa-spin fa-3x fa-fw" style ="display:none"></i></h4>
                        

                  </div>
                 
                </div>

              </div>
            </div> <!-- col-md-4-->

<!--------- VIDEO ---------->
            <div class="col-sm-8" style=" border-radius: 5px 50px 5px 50px; background: rgba(153, 153, 153, 0.54);padding: 10px; margin-right: 5px; height:620px;" >
              
              
              {!! Form::hidden('iduser', $info[0]->id,['id'=>'iduser']) !!}
              <div class="col-sm-12 col-lg-12" align="center">
                <div id='videos'>
                  <!--  <h3>Doctor: </h3> -->

                    <div id="container">
                      <div id="big_video">
                        <video id='remoteVideo' class="remoteVideoIns"  autoplay ></video>
                      </div>
                      <div id="small_video">
                            <video id='localVideo' class="localVideoIns" autoplay muted></video>
                          </div>
             
                    </div>
                  </div>
              </div> 
          </div>   
          </div>
        </div>  
@endsection 

  @push('scripts')

<script src="https://www.doctornow24.com:2905/socket.io/socket.io.js"></script>
     
    <!-- WEB RTC Code -->
    {!! Html::script('assets/js/webrtc.adapter.js') !!}
    {!! Html::script('assets/js/conference.js') !!}

    <script type="text/javascript">
        var sound=$('#sound').val();
        var getUrl = window.location.origin;
        var urlpath=getUrl + "/videcc/public/sounds/";
        ion.sound({
                        sounds: [
                            {name: sound }
                        ],
                        path: urlpath,
                        preload: true,
                        volume: 1.0
                }); 

        var userId ={!! $info[0]->id!!};
      
       var socket = io('https://www.doctornow24.com:2905/');
        var currentCall;
        var conf = new conference({
                sendOfferCallback: function(offer) {
                    if(currentCall) {
                        socket.emit('offer', currentCall.id, offer);
                    }
                },
                sendCandidateCallback: function(evt) {
		  if(currentCall && evt.candidate){
                    if(evt.candidate.candidate.toString().indexOf('typ host') == -1) {
			console.log("sendCandidateCallback",evt.candidate);
			// console.log( evt.candidate.candidate.toString());
                        socket.emit('sendCandidate', currentCall.id, evt.candidate);
                    }
		  }else console.log("Todos los candidatos enviados por el Paciente", evt.candidate);
                }
        });
        
        socket.on('connection', function() { console.log('connected to socket.io'); }); 
        socket.on('disconnect', function() { console.log('disconnected from socket.io'); });
        
	socket.on('sendCandidate', call, function(candidate) {
	   console.log("Entrando por sendCandidate", candidate);	
	   if(candidate){
             socket.emit('beginCall', call.id);
             console.log("llego candidato del Dr: sendCandidate", candidate);
		}else console.log("No llego candidato del Dr: sendCandidate", candidate);
        });

        
        setInterval(function() {
            socket.emit('insurer', userId);
        }, 1000);
        
        
        socket.on('answer', function(call) {
         console.log("Call Answered by doc",call);
           conf.receiveAnswer(call.answer);
           //socket.emit('beginCall', call.id);
        });
        
        socket.on('beginCall', function(call) {
           if(call) {
             var candidates = call.host_candidates;
             console.log("Ice candidates received BeginCall", candidates);
             $("#endcall").show(); $("#wait").hide();$("#waitlabel").hide();
              ion.sound.stop(sound);
             for(i = 0; i < candidates.length; i++) {
                   conf.handleIceCandidate(candidates[i]);
             }
             
           } 
        }); 
        
        socket.on("callCreated", function(call) {
            currentCall = call;
            conf.createCall();
            $("#call").hide(); $("#leave").hide();
            ion.sound.play(sound);
            $("#waitlabel").show();
            $("#wait").show(function(){setTimeout(endcallwait, 30000);});

        });
        
        socket.on("callEnded", function(call) {
        var getUrl = window.location.origin;  
	  currentCall = null;
            $.alert({
                  icon: 'fa fa-warning',
                  title: 'Atencion!',
                  content: "La llamada ha sido finalizada por el Doctor"
                  
              });
          $(location).attr('href',getUrl);
        });
        conf.initialize();
        
         $(document).ready(function($) {
            
            $("#call").click(function(e) {
              socket.emit("createCall");

            });
      

            $("#endcall").click(function(e) {
              var getUrl = window.location.origin; 
               $.confirm({
                      title: 'Quiere finalizar la llamada?',
                      content: '',
                      confirmButton: 'Finalizar',
                      cancelButton: 'Cancelar',
                      confirmButtonClass: 'btn-success',
                      cancelButtonClass: 'btn-danger',
                  confirm: function(){
                     socket.emit("endCall", currentCall.id);
                     currentCall = null;
                    //$("#endcall").hide(); $("#call").show(); $("#leave").show();
                    $(location).attr('href',getUrl);    
                  },
                  
                  cancel: function(){
                     
                  },
                  autoClose: 'cancel|20000'
                });
            });
        });
         function endcallwait(){
          var disp = document.getElementById('wait').style.display;
          if( disp== 'inline-block'){ 
                    socket.emit("endCall", currentCall.id);
                    currentCall = null;
                    $("#wait").hide();$("#waitlabel").hide();$("#endcall").hide(); $("#call").show(); $("#leave").show();
                    ion.sound.stop(sound);
                    $.alert({
                        icon: 'fa fa-warning',
                        title: 'Atencion!',
                        content: "No hay Doctores Disponibles en este momento, por favor intentemas tarde"
                        
                    });
          } 
                  
         }
    </script>

    
 @endpush   
