<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the forms on the app
    | Feel free to change this however you need to do 
    |
    */

   	'login'		  => 'Login',
    'home'        => 'Principal',
    'name'        => 'Nombre',
    'action'      => 'Acciones',
   	'signup'	  => 'Registro Usuario',
    'signup_adm'  => 'Registro Personal',
   	'logout'      => 'Salir',
    'addstaff'    => 'Ingresar Personal',
    'report'      => 'Reportes',
    'search'      => 'Buscar',
    'newoption'   => 'Nueva Opcion',
    'searchoption'=> 'Buscar Opcion',
    'searchrol'   => 'Buscar Rol',
    'newrol'      => 'Nuevo Rol',
    'searchoermission'   => 'Buscar Permiso',
    'newpermiso'      => 'Nuevo Permiso',
    'position'      => 'Posicion'
    

 	
    
];
