<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the forms on the app
    | Feel free to change this however you need to do 
    |
    */

    'login' => [
    	'title'		=> 'Login',
    	'submit'	=> 'Entrar',
        'welcome'   => 'Bienvenido',
        'username'   => 'Nombre de usuario',
        'password'   => 'Contraseña',
    	
    ],
    'signup' => [
    	'title'		=> 'Registro de usuario     ',
        'title_admin'     => '     Registro de Administrador',
    	'submit' 	=> 'Guardar',
        'back'    => 'Volver'
    ],
    	
    'label'	=> [
    	'email'					=> 'Correo Electronico',
        'email2'                => 'Correo Electronico Segundario ',
    	'password'				=> 'Contraseña',
    	'password_confirmation' => 'Confirmar contraseña',
    	'remember'				=> 'Recuérdame',
    	'name'					=> 'Nombres',
        'last_name'             => 'Apellidos',
        'identity'              => 'Cedula',
        'type_staff'            => 'Tipo de Personal',
        'local'                 => 'Su teléfono Local',
        'celular'               => 'Su teléfono Celular',
        'fecha_nac'             => 'Fecha de nacimiento'


    ],
    'email' => [
        'title'     => 'Reenvio de Contraseña',
        'submit'    => 'Solicitar'
    ],
    
];
