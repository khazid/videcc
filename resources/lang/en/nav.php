<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the forms on the app
    | Feel free to change this however you need to do 
    |
    */

   	  'login'     => 'Login',
    'home'        => 'Home',
    'name'        => 'Name',
    'action'      => 'Action',
    'signup'    => 'Signup',
    'signup_adm'  => 'Admin Singup',
    'logout'      => 'Logout',
    'addstaff'    => 'Add Staff',
    'report'      => 'Reports',
    'search'      => 'Buscar',
    'newoption'   => 'New Option',
    'searchoption'=> 'Search Option',
    'searchrol'   => 'Search Rol',
    'newrol'      => 'New Rol',
    'searchoermission'   => 'Search Permission',
    'newpermiso'      => 'New Prmission',
    'position'      => 'Position'
 	
    
];
