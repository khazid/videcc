jQuery(document).on('ready', function(){
	var $ = jQuery;

	$('[data-upload-file-target]').on('change', function() {
		var target = $(this).data('uploadFileTarget');
		$(target).val(this.value);
	})
});