

                 //verificacion de dispositivo movil
                    var isMobile = {
                                Android: function() {
                                    return navigator.userAgent.match(/Android/i);
                                },
                                BlackBerry: function() {
                                    return navigator.userAgent.match(/BlackBerry/i);
                                },
                                iOS: function() {
                                    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                                },
                                Opera: function() {
                                    return navigator.userAgent.match(/Opera Mini/i);
                                },
                                Windows: function() {
                                    return navigator.userAgent.match(/IEMobile/i);
                                },
                                any: function() {
                                    return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
                                }
                            };
                    //-----------------------------

                     $( document ).ready(function() {
                      
                         if (isMobile.any()) {
                             var goFS = document.getElementById("call");
                             goFS.addEventListener("click", function() {
                                JQuery('#small_video').hide();
                                JQuery('#localVideo').hide();
                                toggleFullScreen();
                             }, false);
                         }
                     });

                     
                 function toggleFullScreen() {
                    var videoRemote = document.getElementById("remoteVideo");
                    
                    if (!document.mozFullScreen && !document.webkitFullScreen) {
                      if (videoRemote.mozRequestFullScreen) {
                        videoRemote.mozRequestFullScreen();
                      } else {
                        videoRemote.webkitRequestFullScreen();
                      }

                      
                    } else {
                      if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                      } else {
                        document.webkitCancelFullScreen();
                      }
                      

                    }
                  };
  