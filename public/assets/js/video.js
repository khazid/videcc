        var sound=$('#sound').val();
        var userId = $('#userId').val();
        var getUrl = window.location.origin;
        var urlpath=getUrl + "/videcc/public/sounds/";
        console.log('soundpath: '+urlpath);
        var currentCall;
	    var keep_answer;
	    var sendSubmit=false;
         var socket = io('https://www.doctornow24.com:2905/');
        
      
        
        var conf = new conference({
                sendAnswerCallback: function(offer, answer) {
                    if(currentCall) {
                        socket.emit('answer', currentCall.id, answer);
			
			console.log("Enviando respuesta, Aguantando beginCall hasta tener todos los candidatos");
                    }
                },
                sendCandidateCallback: function(evt) {
                   if(currentCall && evt.candidate){

                       if(evt.candidate.candidate.toString().indexOf('typ host') == -1) {
                    			console.log("sendCandidateCallback DOC",evt.candidate);
                                            socket.emit('sendCandidate', currentCall.id, evt.candidate);
                    			setTimeout(function(){ socket.emit('beginCall', currentCall.id); }, 1500);
                        }else console.log("NO ENVIADO sendCandidateCallback  DOC",evt.candidate);

              		  }else {console.log("Todos los candidatos del DOC enviados", evt.candidate, keep_answer);
              			
              			}
                }

        });
        ion.sound({
                        sounds: [
                            {name: sound }
                        ],
                        path: urlpath,
                        preload: true,
                        volume: 1.0
                });  
        socket.on('connection', function() { console.log('connected to socket.io'); }); 
        socket.on('disconnect', function() { console.log('disconnected from socket.io'); });
        
        setInterval(function() {
            socket.emit('doctor', userId);
        }, 1000);   

        //entra la llamada del paciente   
        socket.on('offer', function(call) {
                
                console.log("offer",call.caller);
                //SE INICIALIZAN LAS VARIABLES PARA EL SONIDO AL MOSTRAR EL ALERT DE LLAMADA
                
        if($('#available').val()=="true"){   
                $('#available').val("false");//se coloca al medico como No Disponible       
                //CONFIRM PARA ATENDER LA LLAMADA
                 $.confirm({
                      title: 'Quieres aceptar la llamada?',
                      content: 'Al aceptar se llenara la informacion del paciente, si cancela la misma podra ser respondida por otro doctor disponible',
                      confirmButton: 'Atender',
                      cancelButton: 'Rechazar',
                      confirmButtonClass: 'btn-success',
                      cancelButtonClass: 'btn-danger',
                  confirm: function(){
                  		captureUserMedia(mediaConstraints, onMediaSuccess, onMediaError);
                      $('#busy').val("true");//coloca como ocupado para llamada telefonica
                      
                      if($('#callcenter').val()=="true"){
                         sipUnRegister();//se sale de la sesion de callcenter patra que no acepte llamadas

                      }
                     

                        start(); //se inicia el cronometro
                        $('#contenedorreloj').show(); //se muestra el cronometro
                        $('#endandsave').removeAttr('disabled');//se activa el boton de guardar
                        
                        //agregar cambio de estado a disponible para llamadas telefonicas
                       
                        
                        currentCall = call;
                        $('#videocallid').val(call.id);
                        //SE INCIAN LAS VARIABLES Y SE ESTABLECE EL TOKEN PARA EL POST EN LARAVEL (EVITA ERROR 500 AL HACER POST)
                            var ul='';
                            var divhist='';
                            var idaseg = call.caller_id;
                            $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                            });

                            //SE INICIA EL AJAX, SE ESTABLECEN LAS VARIABLES Y LA RUTA DONDE ESTA EL CONTROLADOR QUE HARA LA BUSQUEDA EN LA BD    
                            $.ajax({
                                method: 'POST', // Type of response and matches what we said in the route
                                url: 'video/buscar', // This is the url we gave in the route
                                data: {'id' : idaseg}, // a JSON object to send back
                                success: function(response){ // What to do if we succeed
                                console.log("respuesta: " +response.aseg[0]); 

                                $("username").val(response.aseg[0]['name']);
                                $("useremail").val(response.aseg[0]['email']);

                                }// /.SUCCESS
                            });// /.AJAX
									//d = new Date(); //cambiar esto por la fecha del server
                  //console.log(d);
                           //recordTime= d.getFullYear()+'-'+d.getMonth()+'-'+d.getDay()+'_'+d.getHours()+d.getMinutes();
                  specificName='WEB_ic'+currentCall.id; //'docUsername-fecha_hora'
                  iddoc=$('#userId').val();
                  $('#calledfile').val(specificName+'-dc'+iddoc+'.webm');
                            conf.initialize(function() {
                            
                                //SE ESTABLECE LA LLAMADA 
                                conf.acceptCall(call.offer, call.caller.candidates);

                                //Extraer de call.caller las variables que necesitas y meterlas en los divs apropiados.
                                console.log(call.caller, call.offer); 
                            
                            });// /.INITIALIZE
                            ion.sound.stop(sound);
                      },
                      onOpen: function(){
                      ion.sound.play(sound);
                      },
                      cancel: function(){
                          $.alert('Llamada no atendida!');
                          $('#available').val("true");//se coloca al medico como Disponible
                          ion.sound.stop(sound);
                          location.reload();
                      },
                      autoClose: 'cancel|20000'
                  });
          };// /. if available
        }); // /.socket.on('offer', function(call)
        
        socket.on('beginCall', function(call) {
           if(call) {
             var candidates = call.caller_candidates;
             console.log("Ice candidates received", candidates); 
             for(i = 0; i < candidates.length; i++) {
                   conf.handleIceCandidate(candidates[i]);
             }
             //IMPORTANTE 
              console.log("beginCall",call);
              
           } 
        });

        socket.on('answered', function(call) {
           console.log("answered",call);
        });

         socket.on("callCreated", function(call) {
            currentCall = call;
            conf.createCall();
            $("#endcall").show(); $("#call").hide();
        });
        
        socket.on("callEnded", function(call) {
            currentCall = null;
            mediaRecorder.stop();
            mediaRecorder.stream.stop();
            conf.recording();
            alert("La llamada ha sido finalizada por el usuario, Guarde la informacion obtenida ");
				//ifcallended=true;            
        });
        conf.initialize();
        

$(document).ready(function ($) {

  

//-----validacion  antes de guardar-----
  $("#endandsave").click(function(e) {
   
        //en caso de no existir ningun campo vacio se pregunta si quiere finalizar llamada
        var c = confirm('Quiere finalizar la llamada?');
             if(c){
                 (!sendSubmit)? sendSubmit=true : sendc=true;
             	
                  if(callcenter && !busy) {//se desloguea de la central
                     		sipHangUp(); //se cuelga la llamada telefonica
                     		sipUnRegister();
                        //agregar cambio de estado a disponible para llamadas telefonicas
                        $('#available').val("true");//se coloca disponible al medico

                        console.log("SE FINALIZA LLAMADA CENTRAL"); 
                  }else{
                      // stop();//se detiene el cronometro
                     // $('#contenedorreloj').hide();//se esconde el conometro
                      if(currentCall!=null){
                        socket.emit("endCall", currentCall.id); //se termina la llamada
                        currentCall = null;
                         
                        mediaRecorder.stop();
                        mediaRecorder.stream.stop();
                        conf.recording();
                        console.log("SE FINALIZA VIDEO LLAMADA");
                      }

                  }

             }else{
               $.alert('No se finalizo la llamada ni se guardo ninguna informacion!');
             }
        return sendc;

      }
        
                              
                    
}); 

//-------------------------------  

  $('#callerror').click(function(){
      location.reload();
  });           
  
 
}); 

 