/* 
Conference - 2016 
Version: 0.1
Author: Kellerman Rivero (krsloco@gmail.com)
Description: A conference (webrtc) methods for handle videocalls
*/
(function (window, undefined) {

    var extend = function () {

        // Variables
        var extended = {};
        var deep = false;
        var i = 0;
        var length = arguments.length;

        // Check if a deep merge
        if ( Object.prototype.toString.call( arguments[0] ) === '[object Boolean]' ) {
            deep = arguments[0];
            i++;
        }

        // Merge the object into the extended object
        var merge = function (obj) {
            for ( var prop in obj ) {
                if ( Object.prototype.hasOwnProperty.call( obj, prop ) ) {
                    // If deep merge and property is an object, merge properties
                    if ( deep && Object.prototype.toString.call(obj[prop]) === '[object Object]' ) {
                        extended[prop] = extend( true, extended[prop], obj[prop] );
                    } else {
                        extended[prop] = obj[prop];
                    }
                }
            }
        };

        // Loop through each object and conduct a merge
        for ( ; i < length; i++ ) {
            var obj = arguments[i];
            merge(obj);
        }

        return extended;

    };

    var audioBandwidth = 16;
    var videoBandwidth = 96;

    function setBandwidth(sdp) {
        sdp = sdp.toString().replace(/a=mid:audio\r\n/g, 'a=mid:audio\r\nb=AS:' + audioBandwidth + '\r\n');
        sdp = sdp.toString().replace(/a=mid:video\r\n/g, 'a=mid:video\r\nb=AS:' + videoBandwidth + '\r\n');
	console.log("SDP:" + sdp);
        return sdp;
    }

    var settings = {
        localVideoElementId: 'localVideo',
        remoteVideoElementId: 'remoteVideo',
        sendCandidateCallback: function (evt) {
            console.log(["Candidate: ", evt]);
        },
        sendOfferCallback: function (offer) {
            console.log("Oferta:" + offer);
        },
        sendAnswerCallback: function (offer, answer) {
            console.log("Answer:" + answer);
        }
    }

    var _ = {};
    var _localStream;
    var connection;
    var iceCandidates = [];
    
    function error(e) {
        console.warn(e);
    }
    
    function getConnection() {
        var conn = new webrtc.RTCPeerConnection({
             iceTransportPolicy: "all",
		 iceServers: [
                       // { url: "stun:201.249.190.145" },
                      ///  { url: "turn:201.249.190.145", username: "conference", credential: "conference" }
                     //     {url:'stun:stun.l.google.com:19302'},
                     //    {url:'stun:stun1.l.google.com:19302'},
			{url:'stun:numb.viagenie.ca'},
                        {url: 'turn:numb.viagenie.ca', username: 'kamalyacevedo@gmail.com', credential: 'kam5649821'},
                     {url: 'turn:numb.viagenie.ca', username: 'raulj.camacho@gmail.com', credential: 'vgetpa10'}
                      
                ]
        });
        
        conn.onicecandidate = settings.sendCandidateCallback;
        conn.onaddstream = function (evt) {
            webrtc.attachStream(document.getElementById(settings.remoteVideoElementId), evt.stream);
        };
        
        return conn;
    }
    
    function callOffer(create, offer, candidates) {
        
         if(!_localStream) 
         {
             throw "Local stream not initialized";
         }
        
         connection = getConnection();
         connection.addStream(_localStream);
       
         if(create) { //Create
            connection.createOffer(function(offer) {
		offer.sdp=setBandwidth(offer.sdp);
//            	sessionDescription=new webrtc.RTCSessionDescription(offer);
//            	sessionDescription=setBandwidth(sessionDescription);
                    connection.setLocalDescription(new webrtc.RTCSessionDescription(offer), 
                    function () {
                        settings.sendOfferCallback(offer);       
                    }, error);
            }, error);
         } else { //Accept
//		offer.sdp=setBandwidth(offer.sdp);
//         	sessionDescription=new webrtc.RTCSessionDescription(offer);
//                sessionDescription=setBandwidth(sessionDescription);
//	console.log("sessionDescription:" + sessionDescription);
            connection.setRemoteDescription(new webrtc.RTCSessionDescription(offer), function () {
            connection.createAnswer(function (answer) {
//		answer.sdp=setBandwidth(answer.sdp);
//            	sessionDescription=new webrtc.RTCSessionDescription(answer);
//            	sessionDescription=setBandwidth(sessionDescription);
                connection.setLocalDescription(new webrtc.RTCSessionDescription(answer), function () {
                        
                        if(candidates) {
                            candidates.forEach(function(candidate) {
                                    connection.addIceCandidate(new webrtc.RTCIceCandidate(candidate));
                            }, this);
                        }
                        
                        settings.sendAnswerCallback(offer, answer);
                        
                    }, error);
                }, error);
            }, error);
         }
                        
         return connection;
    }
    
    function setAnswer(answer, candidates) {
	answer.sdp=setBandwidth(answer.sdp);
//    	sessionDescription=new webrtc.RTCSessionDescription(answer);
//    	sessionDescription=setBandwidth(sessionDescription);
         connection.setRemoteDescription(new webrtc.RTCSessionDescription(answer), function () {
             if(candidates) {
                    candidates.forEach(function(candidate) {
                            connection.addIceCandidate(new webrtc.RTCIceCandidate(candidate));
                    }, this);
             }
         }, error);
    }
    
    function getLocalVideo(callback){
          webrtc.getUserMedia(
            { "audio": true, "video": true }, 
            function (stream) {
                _localStream = stream;
                webrtc.attachStream(document.getElementById(settings.localVideoElementId), stream);
                
                if(callback) callback();
            },
            error);
    };
    
    window.conference = function (_settings) {
        settings = extend(settings, _settings);
        
        this.initialize = function (callback) {
            getLocalVideo(callback);
        };
        
        this.createCall = function () {
            callOffer(true);
        };
        
        this.acceptCall = function (offer, candidates) {
            callOffer(false, offer, candidates);
        };
        
        this.receiveAnswer = function (answer, candidates) {
            setAnswer(answer, candidates);
        };
        
        this.handleIceCandidate = function (candidate) {
            if(candidate) {
                if (connection.remoteDescription && connection.remoteDescription.sdp && connection.remoteDescription.sdp != "") {
                            connection.addIceCandidate(new webrtc.RTCIceCandidate(candidate));
                } else {
                    iceCandidates.push(candidate);
                }
            }
        };
        
        this.author = new (function() {
            this.FullName = "Kellerman Rivero";
            this.Email =  "krsloco@gmail.com";
            this.Copyright = "2016";
            this.MeetTheAuthor = function() {
                window.location = "https://twitter.com/riverokellerman";
            }
        })();
    };

})(window, undefined);
