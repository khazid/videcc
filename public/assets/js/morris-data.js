$(function() {
            Morris.Donut({
                element: 'chart-donut',
                data: [{
                    label: "Pedro",
                    value: 65
                }, {
                    label: "Maria",
                    value: 30
                }, {
                    label: "Jose",
                    value: 52
                }],
                //data: JSON.parse('<?php echo json_encode($donut) ?>')
            });
            // Bar Chart
            Morris.Bar({
                element: 'chart-bar',
                data: [{
                    period: '2016-05-15',
                    pedro: 20,
                    maria: null,
                    jose: 15
                }, {
                    period: '2016-05-16',
                    pedro: 5,
                    maria: 14,
                    jose: 22
                }, {
                    period: '2016-05-17',
                    pedro: 30,
                    maria: 10,
                    jose: 15
                }, {
                    period: '2016-05-18',
                    pedro: 17,
                    maria: 4,
                    jose: 25
                }, {
                    period: '2016-05-19',
                    pedro: 4,
                    maria: 30,
                    jose: 5
                }, {
                    period: '2016-05-20',
                    pedro: 5,
                    maria: 7,
                    jose: 8
                }, {
                    period: '2016-05-21',
                    pedro: 14,
                    maria: 10,
                    jose: 15
                }, {
                    period: '2016-05-22',
                    pedro: 20,
                    maria: 25,
                    jose: 22
                }],
                //JSON.parse('<?php echo json_encode($data) ?>'),
                xkey: 'period',
                ykeys: ['pedro','maria','jose'],
                labels: ['Pedro','Maria','Jose'],
                hideHover: 'auto',
                resize: true,
                
            });
           // Morris.Line({
           //      element: 'chart-line',
           //      data: [{
           //          period: '2016-05-15',
           //          pedro: 20,
           //          maria: null,
           //          jose: 15
           //      }, {
           //          period: '2016-05-16',
           //          pedro: 5,
           //          maria: 14,
           //          jose: 22
           //      }, {
           //          period: '2016-05-17',
           //          pedro: 30,
           //          maria: 10,
           //          jose: 15
           //      }, {
           //          period: '2016-05-18',
           //          pedro: 17,
           //          maria: 4,
           //          jose: 25
           //      }, {
           //          period: '2016-05-19',
           //          pedro: 4,
           //          maria: 30,
           //          jose: 5
           //      }, {
           //          period: '2016-05-20',
           //          pedro: 5,
           //          maria: 7,
           //          jose: 8
           //      }, {
           //          period: '2016-05-21',
           //          pedro: 14,
           //          maria: 10,
           //          jose: 15
           //      }, {
           //          period: '2016-05-22',
           //          pedro: 20,
           //          maria: 25,
           //          jose: 22
           //      }],
           //      //data: JSON.parse('<?php echo json_encode($data) ?>'),
           //      parseTime: false,
           //      xkey: 'period',
           //      ykeys: ['pedro','maria','jose'],
           //      labels: ['Pedro','Maria','Jose'],
           //      pointSize: 2,
           //      hideHover: 'auto',
           //      resize: true,
           //  });
 
           //  Morris.Area({
           //      element: 'chart-area',
           //      //JSON.parse('<?php echo json_encode($data) ?>'),
           //      data: [{
           //          period: '2016-05-15',
           //          pedro: 20,
           //          maria: null,
           //          jose: 15
           //      }, {
           //          period: '2016-05-16',
           //          pedro: 5,
           //          maria: 14,
           //          jose: 22
           //      }, {
           //          period: '2016-05-17',
           //          pedro: 30,
           //          maria: 10,
           //          jose: 15
           //      }, {
           //          period: '2016-05-18',
           //          pedro: 17,
           //          maria: 4,
           //          jose: 25
           //      }, {
           //          period: '2016-05-19',
           //          pedro: 4,
           //          maria: 30,
           //          jose: 5
           //      }, {
           //          period: '2016-05-20',
           //          pedro: 5,
           //          maria: 7,
           //          jose: 8
           //      }, {
           //          period: '2016-05-21',
           //          pedro: 14,
           //          maria: 10,
           //          jose: 15
           //      }, {
           //          period: '2016-05-22',
           //          pedro: 20,
           //          maria: 25,
           //          jose: 22
           //      }, {
           //          period: '2016-05-23',
           //          pedro: 20,
           //          maria: null,
           //          jose: 15
           //      }, {
           //          period: '2016-05-24',
           //          pedro: 2,
           //          maria: 30,
           //          jose: 25
           //      }],
           //      xkey: 'period',
           //      ykeys: ['pedro', 'maria', 'jose'],
           //      labels: ['Pedro', 'Maria', 'Jose'],
           //      pointSize: 2,
           //      hideHover: 'auto',
           //      resize: true
                
           //  });

            

});
