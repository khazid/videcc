<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimpl5Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('sipml5_call_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('video');
            $table->boolean('rtcweb_breaker');
            $table->string('websocket_url',32);
            $table->string('sipoutbound_url',32);
            $table->string('ice_servers');
            $table->string('max_bandwidth',32);
            $table->string('video_size');
            $table->boolean('early_ims_3gpp');
            $table->boolean('debug_messages');
            $table->boolean('en_cache');
            $table->boolean('callbutton_options');
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sipml5_call_settings');
    }
}
