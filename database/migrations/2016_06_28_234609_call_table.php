<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         
        Schema::create('calls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('recipient_id')->nullable();
            $table->integer('caller_id');
            $table->integer('status');
            $table->string('caller_name',255);
            $table->string('caller_identity_number',255);
            $table->integer('history_id')->nullable();
            $table->string('caller_file')->nullable();
            $table->string('called_file')->nullable();
            $table->timestamp('ended_at')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('calls');
    }
}
