<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIppbxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ippbx_connection', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->foreign('id_user')->references('id')->on('users');
            $table->string('name',20);
            $table->string('priv_identity',32);
            $table->string('pub_identity',32);
            $table->string('password');
            $table->string('realm',32);
            $table->nullableTimestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ippbx_connection');
    }
}
