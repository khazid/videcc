<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallcenterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('callcenter', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->foreign('id_user')->references('id')->on('users');
            $table->integer('insured_id');
            $table->foreign('insured_id')->references('id')->on('insured');
            $table->timestamp('ended_at')->nullable();
            $table->integer('history_id')->nullable();
            $table->string('caller_file')->nullable();
            $table->string('called_file')->nullable();
            $table->nullableTimestamps();
            $table->softDeletes();

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('callcenter');
    }
}
