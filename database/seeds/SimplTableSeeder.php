<?php

use Illuminate\Database\Seeder;

class SimplTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sipml5_call_settings')->insert([
            'id' => 1,
            'video' => TRUE,
            'rtcweb_breaker' => TRUE,
            'websocket_url' => 'ws://192.168.8.5:8088/ws',
            'sipoutbound_url' => 'udp://192.168.8.5:5060',
            'ice_servers' => NULL,
            'max_bandwidth' => NULL,
            'video_size' => NULL,
            'early_ims_3gpp' => FALSE,
            'debug_messages' => FALSE,
            'en_cache' => FALSE,
            'callbutton_options' => FALSE,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
    }
}
