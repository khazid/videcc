<?php

use Illuminate\Database\Seeder;

class MapSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('map')->insert(['name' =>'Hospital de clinicas Caracas','lat' => '10.5099826','lng' => '-66.8988654','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        
        DB::table('map')->insert(['name' =>'Hospital Victorino Santaella','lat' => '10.3542817','lng' => '-67.036406','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        
        DB::table('map')->insert(['name' =>'Clinica Sanatrix','lat' => '10.4702180399803','lng' => '-66.8457467583984','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

        DB::table('map')->insert(['name' =>'Locatel Sebucan ','lat' => '10.497782','lng' => '-66.8337888','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

        DB::table('map')->insert(['name' =>'Farmatodo Santa Eduvigis','lat' => '10.5005685','lng' => '-66.8407345','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
    }
}
