<?php

use Illuminate\Database\Seeder;

class IppbxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ippbx_connection')->insert([
            'id' => 1,
            'id_user' => 1,
            'name' => '1010',
            'priv_identity' => '1010',
            'pub_identity' => 'sip:1010@192.168.8.5',
            'password' => 'pt1234',
            'realm' => 'grandstream',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
    }
}
