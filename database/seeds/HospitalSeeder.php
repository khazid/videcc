<?php

use Illuminate\Database\Seeder;

class HospitalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('hospital')->insert(['name' =>'Hospital de clinicas Caracas','address' =>'Caracas, Avenida panteon','map_id' => '1','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
         DB::table('hospital')->insert(['name' =>'Hospital Victorino Santaella','address' =>'Avenida Bicentenaria, Los Teques','map_id' => '2','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
         DB::table('hospital')->insert(['name' =>'Clinica Sanatrix','address' =>'Campo Alegre, Caracas','map_id' => '3','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        
    }
}
