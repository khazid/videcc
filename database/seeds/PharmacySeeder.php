<?php

use Illuminate\Database\Seeder;

class PharmacySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('pharmacy')->insert(['name' =>'Locatel Sebucan','address' =>'Av. Sucre los dos caminos, Caracas','map_id' => '4','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

       DB::table('pharmacy')->insert(['name' =>'Farmatodo Santa Eduvigis','address' =>'1ra Av. con 2da transversal ( Una Cuadra Hacia el Sur de la Iglesia Sta. Edivigis)','map_id' => '5','created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

    }
}
