<?php

use Illuminate\Database\Seeder;
use Bican\Roles\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('permissions')->insert([
            'name' => 'Busqueda de medicamento',
            'slug' => '#',
            'description' => 'Buscar medicamentos en la red de farmacias',
            'menu_id' => 5,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'Opciones del Menu',
           'slug' => 'menu.index',
            'description' => 'administrar las opciones del menu principal del sistema',
            'menu_id' => 6,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'Roles',
            'slug' => 'rol.index',
            'description' => 'Administrar roles del sistema',
             'menu_id' => 6,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'Permisos',
            'slug' => 'perm.index',
            'description' => 'Permisos del sistema',
             'menu_id' => 6,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'Personal',
            'slug' => 'personal.index',
            'description' => 'Ingresar personal nuevo al sistema',
            'menu_id' => 2,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'Hospital/Clinica',
            'slug' => 'hospital.index',
            'description' => 'Ingresar Hospitales y clinicas afiliadas al sistema',
            'menu_id' => 6,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'Farmacias',
            'slug' => 'pharmacy.index',
            'description' => 'Ingresar Farmacias afiliadas al sistema',
            'menu_id' => 6,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'Buscar Farmacias',
            'slug' => 'pharmacy.show',
            'description' => 'Buscar Farmacias afiliadas al sistema',
            'menu_id' => 5,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'Buscar Hospitales / Clinicas',
            'slug' => 'hospital.show',
            'description' => 'Buscar Hospitales y Clinicas afiliadas al sistema',
            'menu_id' => 5,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('permissions')->insert([
            'name' => 'Informacion de Asegurado',
            'slug' => 'search.index',
            'description' => 'Buscar Informacion del asegurado area administrativa',
            'menu_id' => 5,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
         DB::table('permissions')->insert([
            'name' => 'Consulta online',
            'slug' => 'video.index',
            'description' => 'Video llamada',
            'menu_id' => 4,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
         DB::table('permissions')->insert([
            'name' => 'Aseguradoras',
            'slug' => 'insurance.index',
            'description' => 'Agregar Aseguradoras al sistema',
            'menu_id' => 6,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
         DB::table('permissions')->insert([
            'name' => 'Buscar Asegurado',
            'slug' => 'pharmacy/findinsured',
            'description' => 'Buscar informacion de Asegurados para las farmacias',
            'menu_id' => 5,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
          DB::table('permissions')->insert([
            'name' => 'Reporte de Llamadas',
            'slug' => 'report.index',
            'description' => 'Reporte de Llamadas recibidas',
            'menu_id' => 1,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
          DB::table('permissions')->insert([
            'name' => 'Usuarios Registrados',
            'slug' => 'user.index',
            'description' => 'Usuarios Registrados en el sistema',
            'menu_id' => 2,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
          DB::table('permissions')->insert([
            'name' => 'Personal Eliminado',
            'slug' => 'personal.searchdeleted',
            'description' => 'Personal eliminado en el sistema',
            'menu_id' => 2,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
       
    }
}
