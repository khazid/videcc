<?php

use Illuminate\Database\Seeder;

class MenuRolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu_role')->insert(['role_id' => 1,'menu_id' => 1,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 1,'menu_id' => 2,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 1,'menu_id' => 3,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 1,'menu_id' => 4,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 1,'menu_id' => 5,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
        DB::table('menu_role')->insert(['role_id' => 1,'menu_id' => 6,'active' => TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

       
    }
}
