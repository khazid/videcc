<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menu')->insert([
            'name' => 'Reportes',
            'position' => 1,
           	'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('menu')->insert([
            'name' => 'Usuarios',
            'position' => 5,
           	'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('menu')->insert([
            'name' => 'Registros',
            'position' => 3,
           	'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('menu')->insert([
            'name' => 'Solicitudes',
            'position' => 4,
           	'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('menu')->insert([
            'name' => 'Busqueda',
            'position' => 2,
           	'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('menu')->insert([
            'name' => 'Configuracion',
            'position' => 6,
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
    }
}
