<?php

use Illuminate\Database\Seeder;

class PermissionRolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>1,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>2,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>3,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>4,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>5,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>6,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>7,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>8,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>9,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>10,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>11,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>12,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>13,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>14,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>15,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>1,'permission_id' =>16,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);


DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>1,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>2,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>3,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>4,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>5,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>6,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>7,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>8,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>9,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>10,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>11,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>12,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),];
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>13,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>14,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>15,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>2,'permission_id' =>16,'active' =>TRUE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);

DB::table('permission_role')->insert(['role_id' =>3,'permission_id' =>1,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>3,'permission_id' =>2,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>3,'permission_id' =>3,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>3,'permission_id' =>4,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>3,'permission_id' =>5,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>3,'permission_id' =>6,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>3,'permission_id' =>7,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>3,'permission_id' =>8,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>3,'permission_id' =>9,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>3,'permission_id' =>10,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>3,'permission_id' =>11,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>3,'permission_id' =>12,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);
DB::table('permission_role')->insert(['role_id' =>3,'permission_id' =>13,'active' =>FALSE,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d'),]);





    }
}
