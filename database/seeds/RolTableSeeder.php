<?php

use Illuminate\Database\Seeder;
use Bican\Roles\Models\Role;

class RolTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     	$adminRole = Role::create([
			'name' => 'Admin',
			'slug' => 'admin',
			'description' => '', // optional
			'level' => 1, // optional, set to 1 by default
		]);

		$staffRole = Role::create([
			'name' => 'Usuario',
			'slug' => 'staff',
			'description' => 'usuario', // optional
			'level' => 2, // optional, set to 1 by default
		]);

		$insuredRole = Role::create([
			'name' => 'Personal',
			'slug' => 'Personal',
			'description' => 'personal', // optional
			'level' => 3, // optional, set to 1 by default
		]);

		

    }
}
