<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
            'name' => 'Administrator',
            'email' => 'admin@drnow.com',
            'password' => bcrypt('123'),
            'created_at' => date('Y-m-d'),
            'active' => TRUE,
            'identity_number' =>'19555666',
            'updated_at' => date('Y-m-d'),
        ]);
       DB::table('users')->insert([
            'name' => 'Staff',
            'email' => 'staff@drnow.com',
            'password' => bcrypt('123'),
            'active' => TRUE,
            'identity_number' =>'19555667',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        
       DB::table('users')->insert([
            'name' => 'Aseguradora',
            'email' => 'aseguradora@drnow.com',
            'password' => bcrypt('123'),
            'active' => TRUE,
            'identity_number' =>'19555668',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
       DB::table('users')->insert([
            'name' => 'Doctor',
            'email' => 'doctor@drnow.com',
            'password' => bcrypt('123'),
            'active' => TRUE,
            'identity_number' =>'19555661',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
       DB::table('users')->insert([
            'name' => 'Doctor2',
            'email' => 'doctor2@drnow.com',
            'password' => bcrypt('123'),
            'active' => TRUE,
            'identity_number' =>'19555662',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
       DB::table('users')->insert([
            'name' => 'Doctor3',
            'email' => 'doctor3@drnow.com',
            'password' => bcrypt('123'),
            'active' => TRUE,
            'identity_number' =>'19555663',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
       DB::table('users')->insert([
            'name' => 'Doctor4',
            'email' => 'doctor4@drnow.com',
            'password' => bcrypt('123'),
            'active' => TRUE,
            'identity_number' =>'19555664',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
       DB::table('users')->insert([
            'name' => 'Farmaceuta',
            'email' => 'farma@drnow.com',
            'password' => bcrypt('123'),
            'active' => TRUE,
            'identity_number' =>'19555665',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);

       DB::table('users')->insert([
            'insurance_id' =>1,
            'countrie_id' =>83,
            'name' => 'Maria Mallorca',
            'email' => 'kamalyacevedo@gmail.com',
            'password' => bcrypt('123'),
            'active' => TRUE,
            'identity_number' =>'12345678',
            'local_number' =>'02125454545',
            'cel_number' =>'04244545454',
            'sexo' =>'f',
            'birth_date' =>'1983-03-18',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
       DB::table('users')->insert([
            'insurance_id' =>1,
            'countrie_id' =>83,
            'name' => 'Pedro Mallorca',
            'email' => 'edpq21@gmail.com',
            'password' => bcrypt('123'),
            'active' => TRUE,
            'identity_number' =>'12345679',
            'local_number' =>'02125454545',
            'cel_number' =>'04244545454',
            'sexo' =>'m',
            'birth_date' =>'1983-03-18',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
       DB::table('users')->insert([
            'insurance_id' =>1,
            'countrie_id' =>83,
            'name' => 'Jose Mallorca',
            'email' => 'lenynguerrero@gmail.com',
            'password' => bcrypt('123'),
            'active' => TRUE,
            'identity_number' =>'12345680',
            'local_number' =>'02125454545',
            'cel_number' =>'04244545454',
            'sexo' =>'m',
            'birth_date' =>'1983-03-18',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
       DB::table('users')->insert([
            'insurance_id' =>1,
            'countrie_id' =>83,
            'name' => 'Juan Mallorca',
            'email' => 'raulj.camacho@gmail.com',
            'password' => bcrypt('123'),
            'active' => TRUE,
            'identity_number' =>'12345681',
            'local_number' =>'02125454545',
            'cel_number' =>'04244545454',
            'sexo' =>'m',
            'birth_date' =>'1983-03-18',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);

      
    }
}
