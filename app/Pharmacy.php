<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pharmacy extends Model
{
	use SoftDeletes;
    protected $table = 'pharmacy';
  	protected $fillable = ['name','address','ubication'];
  	protected $guarded = ['id'];
  	protected $dates = ['deleted_at'];

  	public function insurance()
    {
        return $this->belongsToMany('App\Insurance', 'insurance_pharmacy','pharmacy_id','insurance_id');
    }
  	
}
