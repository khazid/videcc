<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hospital extends Model
{
    use SoftDeletes;
    protected $table = 'hospital';
  	protected $fillable = ['name','address','ubication'];
  	protected $guarded = ['id'];
  	protected $dates = ['deleted_at'];

  	public function insurance()
    {
        return $this->belongsToMany('App\Insurance', 'insurance_hospital','hospital_id','insurance_id');
    }
  	
}
