<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ippbx extends Model
{
    use SoftDeletes;
    protected $table = 'ippbx_connection';
  	protected $guarded = ['id','id_user'];
  	protected $fillable = ['name','priv_identity','pub_identity','password','realm'];
    protected $dates = ['deleted_at'];
}
