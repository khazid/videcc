<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class sipml5 extends Model
{
    use SoftDeletes;
    protected $table = 'sipml5_call_settings';
  	protected $guarded = ['id'];
  	protected $fillable = ['video','rtcweb_breaker','websocket_url','sipoutbound_url','ice_servers','max_bandwidth','video_size','early_ims_3gpp','debug_messages','en_cache','callbutton_options'];
    protected $dates = ['deleted_at'];
}
