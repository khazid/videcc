<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::get('/', 'ActivationController@getFindInsured');
Route::get('/home', 'ActivationController@getFindInsured');
Route::get('home/index', ['as' =>'home/index', 'uses' => 'HomeController@index']);
Route::get('home/enter', ['as' =>'home/enter', 'uses' => 'HomeController@enter']);
Route::get('home/search', ['as' =>'home/search', 'uses' => 'HomeController@search']);
Route::get('github', ['as' =>'github', 'uses' => 'PdfController@github']);
Route::get('downloadpdf/{id}/{hist}/{doc}', ['as' =>'downloadpdf', 'uses' => 'PdfController@downloadpdf']);


//ruta para realizar busqueda de hospitales en la web.
Route::get('home/hospitals', ['as' => 'home/hospitals', 'uses'=>'HomeController@hospitals']);
Route::get('home/hospitalssearch', ['as' => 'home/hospitalssearch', 'uses'=>'HomeController@hospitalssearch']);
Route::get('home/show', ['as' => 'legal', 'uses'=>'HomeController@show']);
Route::get('home/adhesion', ['as' => 'adhesion', 'uses'=>'HomeController@adhesion']);


// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', ['as' =>'auth/login', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('auth/logout', ['as' => 'auth/logout', 'uses' => 'Auth\AuthController@getLogout']);

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', ['as' => 'auth/register', 'uses' => 'Auth\AuthController@postRegister']);
//Registro de Admin
Route::get('auth/findstaff', 'Auth\AuthController@getRegisterAdm');
Route::post('auth/registerAdm', ['as' => 'auth/registerAdm', 'uses' => 'Auth\AuthController@postRegisterAdm']);

// Password reset link request routes...
Route::get('password/email', ['as' => 'password/email', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('password/email', ['as' => 'password/postEmail', 'uses' => 'Auth\PasswordController@postEmail']);

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::get('reset', ['as' => 'personal/reset', 'uses' =>  'PersonalController@reset']);
Route::post('update', ['as' => 'personal/update', 'uses' =>  'PersonalController@updatepass']);



//rutas para el recurso Insured
Route::resource('insured','InsuredController');

//rutas para Busqueda
Route::resource('search','SearchController');
//una nueva ruta para guardar las opciones de los roles 
Route::get('search/store/{id}', ['as' => 'search/store', 'uses'=>'SearchController@store']);
//ruta para realizar busqueda de registros.
Route::post('search/search', ['as' => 'search/search', 'uses'=>'SearchController@search']);

//rutas de video
Route::post('video/buscar','VideoController@buscar');
Route::post('video/buscaraseg','VideoController@buscaraseg');

Route::resource('video','VideoController');


//rutas de report
Route::post('approveclaim', ['as' => 'report/approveclaim', 'uses'=>'ReportController@approveclaim']);
Route::post('background', ['as' => 'report/background', 'uses'=>'ReportController@background']);
Route::post('history', ['as' => 'report/history', 'uses'=>'ReportController@history']);
Route::post('report/siniestros', ['as' => 'report/siniestros', 'uses'=>'ReportController@siniestros']);
Route::post('report/calls', ['as' => 'report/calls', 'uses'=>'ReportController@calls']);
Route::get('report/showcalls', ['as' => 'report/showcalls', 'uses'=>'ReportController@showcalls']);
Route::resource('report','ReportController');

//ruta prueba sipml5
Route::get('sipml5route', function () {
return view('drsipml5/call');
});
Route::get('sip','sipml5@index');

Route::post('uploadAudio', ['as' => 'uploadAudio', 'uses'=>'UploadFileController@saveFile'] );

include_once("Routes/activation.php");
include_once("Routes/configuration.php");
include_once("Routes/pharmacy.php");
include_once("Routes/personal.php");
include_once("Routes/insurance.php");


