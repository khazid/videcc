<?

//una nueva ruta para mostrar el mapa farmacias
Route::get('pharmacy/map/{id}', ['as' => 'pharmacy/map', 'uses'=>'PharmacyController@map']);
//ruta para la busqueda de asegurado
Route::post('pharmacy/insuredsearch', ['as' => 'pharmacy/insuredsearch', 'uses'=>'PharmacyController@insuredsearch']);
//ruta para mostrar la busqueda de asegurado
Route::get('pharmacy/findinsured', ['as' => 'pharmacy/findinsured', 'uses'=>'PharmacyController@findinsured']);
//rutas para Farmacias
Route::resource('pharmacy','PharmacyController');
//una nueva ruta para guardar las opciones de los roles 
Route::get('pharmacy/store/{id}', ['as' => 'pharmacy/store', 'uses'=>'PharmacyController@store']);
//ruta para realizar busqueda de registros.
Route::post('pharmacy/search', ['as' => 'pharmacy/search', 'uses'=>'PharmacyController@search']);
//una nueva ruta para eliminar registros con el metodo get
Route::get('pharmacy/destroy/{id}', ['as' => 'pharmacy/destroy', 'uses'=>'PharmacyController@destroy']);
//una nueva ruta para eliminar registros con el metodo get
Route::get('pharmacy/update/{id}', ['as' => 'pharmacy/update', 'uses'=>'PharmacyController@update']);

//rutas para Hospitales
Route::resource('hospital','HospitalController');
//una nueva ruta para guardar las opciones de los roles 
Route::get('hospital/store/{id}', ['as' => 'hospital/store', 'uses'=>'HospitalController@store']);
//ruta para realizar busqueda de registros.
Route::post('hospital/search', ['as' => 'hospital/search', 'uses'=>'HospitalController@search']);
//una nueva ruta para eliminar registros con el metodo get
Route::get('hospital/destroy/{id}', ['as' => 'hospital/destroy', 'uses'=>'HospitalController@destroy']);
//una nueva ruta para eliminar registros con el metodo get
Route::get('hospital/update/{id}', ['as' => 'hospital/update', 'uses'=>'HospitalController@update']);
//una nueva ruta para mostrar el mapa
Route::get('hospital/map/{id}', ['as' => 'hospital/map', 'uses'=>'HospitalController@map']);

//rutas para Mapas
Route::resource('map','MapController');
//ruta para realizar busqueda de Mapas.
Route::post('map/search', ['as' => 'map/search', 'uses'=>'MapController@search']);
//ruta para realizar busqueda de Mapas.
Route::get('map/add/{id}/{tipo}', ['as' => 'map/add', 'uses'=>'MapController@add']);
//una nueva ruta para eliminar Mapas
Route::get('map/destroy/{id}', ['as' => 'map/destroy', 'uses'=>'MapController@destroy']);
//para actualizar info de los mapas
Route::get('map/update/{id}', ['as' => 'map/update', 'uses'=>'MapController@update']);
//para actualizar info de los mapas
Route::get('map.showphar', ['as' => 'map/showphar', 'uses'=>'MapController@showphar']);