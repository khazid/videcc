<? 

//Personal Staff
//ruta para realizar activacion de personal eliminado
Route::get('personal/searchdeleted', ['as' => 'personal/searchdeleted', 'uses'=>'PersonalController@searchdeleted']);
//ruta para realizar busqueda de registros.
Route::get('personal/activestaff/{id}', ['as' => 'personal/activestaff', 'uses'=>'PersonalController@activestaff']);

//rutas para el recurso del Personal
Route::resource('personal','PersonalController');
//una nueva ruta para eliminar registros con el metodo get
Route::get('personal/destroy/{id}', ['as' => 'personal/destroy', 'uses'=>'PersonalController@destroy']);
//ruta para realizar busqueda de registros.
Route::post('personal/search', ['as' => 'personal/search', 'uses'=>'PersonalController@search']);

//ruta para realizar busqueda de registros en la Tabla Personal.
Route::get('auth/registerStaff/{id}', ['as' => 'auth/registerStaff', 'uses'=>'PersonalController@getRegisterStaff']);
Route::post('personal/registerStaff', ['as' => 'personal/registerStaff', 'uses'=>'PersonalController@registerStaff']);
//ruta para realizar busqueda por poliza y el de registro del asegurado.
Route::get('auth/findinsured', 'PersonalController@getRegisterInsured');
Route::post('personal/registerinsured', ['as' => 'personal/registerinsured', 'uses'=>'PersonalController@registerInsured']);