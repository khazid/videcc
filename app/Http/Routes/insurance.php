<?
//rutas para Aseguradoras
Route::resource('insurance','InsuranceController');
//una nueva ruta para guardar las aseguradoras 
Route::get('insurance/store/{id}', ['as' => 'insurance/store', 'uses'=>'InsuranceController@store']);
//ruta para realizar busqueda de registros.
Route::post('insurance/search', ['as' => 'insurance/search', 'uses'=>'InsuranceController@search']);
//una nueva ruta para eliminar aseguradoras
Route::get('insurance/destroy/{id}', ['as' => 'insurance/destroy', 'uses'=>'InsuranceController@destroy']);