<?


//rutas para el recurso menu
Route::resource('menu','MenuController');
//una nueva ruta para guardar las opciones del menu 
Route::get('menu/create/{id}', ['as' => 'menu/create', 'uses'=>'MenuController@create']);
//ruta para realizar busqueda de registros.
Route::post('menu/search', ['as' => 'menu/search', 'uses'=>'MenuController@search']);
//una nueva ruta para eliminar registros con el metodo get
Route::get('menu/destroy/{id}', ['as' => 'menu/destroy', 'uses'=>'MenuController@destroy']);

//rutas para el recurso Rol
Route::resource('rol','RolController');
//una nueva ruta para guardar las opciones del menu 
Route::get('rol/create/{id}', ['as' => 'rol/create', 'uses'=>'RolController@create']);
//ruta para realizar busqueda de registros.
Route::post('rol/search', ['as' => 'rol/search', 'uses'=>'RolController@search']);
//una nueva ruta para eliminar registros con el metodo get
Route::get('rol/destroy/{id}', ['as' => 'rol/destroy', 'uses'=>'RolController@destroy']);
//una nueva ruta para editar 
Route::get('rol/{id}/edit', ['as' => 'edit', 'uses'=>'RolController@edit']);

//rutas para la asignacion del rol del usuario
Route::resource('rol_user','RoleUserController');
//Users Route
Route::resource('user','UserController');

//rutas para MenuRol
Route::resource('menurol','MenuRolController');
//una nueva ruta para guardar las opciones de los roles 
Route::get('menurol/store/{id}', ['as' => 'menurol/store', 'uses'=>'MenuRolController@store']);
//ruta para realizar busqueda de registros.
Route::post('menurol/search', ['as' => 'menurol/search', 'uses'=>'MenuRolController@search']);
//una nueva ruta para eliminar registros con el metodo get
Route::get('menurol/destroy/{id}', ['as' => 'menurol/destroy', 'uses'=>'MenuRolController@destroy']);
//una nueva ruta para update registros con el metodo get
Route::get('menurol/update/{id}', ['as' => 'menurol/update', 'uses'=>'MenuRolController@update']);
//una nueva ruta para editar registros con el metodo get
Route::get('menurol/{id}/edit', ['as' => 'menurol/edit', 'uses'=>'MenuRolController@update']);

//rutas para Permisos
Route::resource('perm','PermissionsController');
//una nueva ruta para guardar las opciones de los roles 
Route::get('permissions/store/{id}', ['as' => 'perm/store', 'uses'=>'PermissionsController@store']);
//ruta para realizar busqueda de registros.
Route::post('permissions/search', ['as' => 'perm/search', 'uses'=>'PermissionsController@search']);
//una nueva ruta para eliminar registros con el metodo get
Route::get('permissions/destroy/{id}', ['as' => 'perm/destroy', 'uses'=>'PermissionsController@destroy']);

//rutas para PermissionRol
Route::resource('permrol','PermissionRolController');
//una nueva ruta para guardar las opciones de los roles 
Route::get('permissionrol/store/{id}', ['as' => 'permrol/store', 'uses'=>'PermissionRolController@store']);
//ruta para realizar busqueda de registros.
Route::post('permissionrol/search', ['as' => 'permrol/search', 'uses'=>'PermissionRolController@search']);
//una nueva ruta para eliminar registros con el metodo get
Route::get('permissionrol/destroy/{id}', ['as' => 'permrol/destroy', 'uses'=>'PermissionRolController@destroy']);
//una nueva ruta para eliminar registros con el metodo get
Route::get('permissionrol/update/{id}', ['as' => 'permrol/update', 'uses'=>'PermissionRolController@update']);