<?php
	//Rutas para Usuarios del Sistema
	Route::get('activationstaff', ['as' => 'activation.find', 'uses' => 'ActivationController@getFind']);
	Route::post('activationstaff', ['as' => 'activation.find', 'uses' => 'ActivationController@postFind']);

	Route::get('registerstaff/{identity_number}', ['as' => 'activation.register', 'uses' => 'ActivationController@getRegister']);
	
	Route::post('validate-staff/{identity_number}', ['as' => 'validate-staff', 'uses' => 'ActivationController@postValidateStaff']);

	//Registro de Staff
	Route::post('active-staff/{identity_number}', ['as' => 'register-staff', 'uses' => 'ActivationController@postRegisterStaff']);


	//Rutas para la activacion de los asegurados
	Route::get('activationinsured', ['as' => 'activationinsured.find', 'uses' => 'ActivationController@getFindInsured']);
	Route::post('activationinsured', ['as' => 'activationinsured.find', 'uses' => 'ActivationController@postFindInsured']);


	Route::post('validate-insured', ['as' => 'validate-insured', 'uses' => 'ActivationController@postValidateInsured']);
	

	Route::post('registerinsured/{name}/{email}', ['as' => 'activation.register-insured', 'uses' => 'ActivationController@postRegisterInsured']);

	//Subir Archivos
	Route::get('uploadfiles', ['as' => 'upload.files', 'uses' => 'UploadFilesController@getUpload']);
	Route::post('uploadfiles', ['as' => 'upload.files', 'uses' => 'UploadFilesController@postUpload']);


//Rutas para la actiprueba de video llamada doctor
	Route::get('viewdoc', ['as' => 'viewdoc', 'uses' => 'ActivationController@getView']);

	Route::get('videoacepted', ['as' => 'videoacepted', 'uses' => 'ActivationController@videoacepted']);


//Rutas para el detalle del historial
	Route::get('reportinsured/{id}', ['as' => 'reportinsured', 'uses' => 'InsuredController@getReport']);
	Route::get('history/detail/{id}', ['as' => 'history/detail', 'uses' => 'InsuredController@getHistoryDetail']);

//Rutas Mobile
	Route::get('mobile', ['as' => 'mobileview', 'uses' => 'ActivationController@getFindInsuredMobile']);

