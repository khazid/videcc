<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Insurance;
use App\Hospital;
use App\Countries;
use App\Pharmacy;

class HomeController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return \View::make('home');
    }
    public function enter()
    {   
        $hospital = Hospital::join('map', 'map.id', '=', 'hospital.map_id')
                                ->select('hospital.id','hospital.name', 'hospital.address','map.lat','map.lng')
                                ->get();
        $farm = Pharmacy::join('map', 'map.id', '=', 'pharmacy.map_id')
                                ->select('pharmacy.id','pharmacy.name', 'pharmacy.address','map.lat','map.lng')
                                ->get();    
        $countrie= Countries::orderBy('name')->lists('name','id');                                             
        return \View::make('welcome',compact('hospital','farm','countrie'));
    }

    public function search()
    {
         $countrie = Countries::orderBy('name')->lists('name','id');
         $insurance = Insurance::lists('name','id');
         $insurance->prepend('Seleccione', 'null');
         return \View::make('hospital/search',compact('countrie','insurance'));
    }

    public function hospitals(Requests $request)
    {
        $countrie = Countries::orderBy('name')->lists('name','id');
        $insurance = Insurance::where('countrie_id','=',$request->countrie_id)->lists('name','id');
        $insurance->prepend('Seleccione', 'null');
        $hospital = Hospital::where('insurance_id','=',$request->insurance_id)->get();
        return \View::make('hospital/search',compact('insurance','hospital','countrie'));
    }

    public function hospitalssearch(Requests $request)
    {
         $hosp = Hospital::where('name','like','%'.$request->name.'%')->get();
         return \View::make('hospital/search', compact('hosp'));
    }

    public function show()
    {
         return \View::make('terminos');
    }

    public function adhesion()
    {
         return \View::make('contrato_adhesion');
    }
}
