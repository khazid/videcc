<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pharmacy;
use App\Map;
use App\Insurance;
use Input;
use App\Insured;
use App\User;

class PharmacyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    public function index()
    {
        $pharmacy = Pharmacy::all();
         $tipo='config'; 
         $title='Administracion de Farmacias';
        return \View::make('pharmacy/list',compact('pharmacy','tipo','title'));
        //return \View::make('tables',compact('pharmacy','tipo','title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $asegura= Insurance::all();
        $tipo='ph';
        return \View::make('pharmacy.new',compact('asegura','tipo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //para guardar la info del hospital en la tabla
       $phar = new Pharmacy;
       $phar->name = $request->name;
       $phar->address = $request->addressh;
       $phar->map_id = 0;
       $phar->save();
      
       //se busca el id que se acaba de insertar
       $pid= $phar->id;
       
        //se guarda la informacion del mapa
       $map = new Map;
        $map->name = $request->name;
        $map->lat = $request->lat;
        $map->lng = $request->lng;
        $map->save();

       // se busca el ultimo id del mapa guardado
       $maps=$map->id;
        
        // se actualiza la tabla de hospital con el id del mapa
           $phar->map_id=$maps;
           $phar->save();
            
        //asignacion del hospital a la aseguradoras correspondientes

          $checkbox = Input::get('insurance');
            
             foreach($checkbox as $c){
                 
               if(substr($c, 0,1)  != 'i'){ 
                    $idi=substr($c,0,1);
                    
                    $phar->insurance()->attach($idi,['created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d')]);
                }
                
             }
            
        //----------------------------------------
        
        return redirect('pharmacy');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function map($id)
    {
        $hosp = Pharmacy::find($id);
        $map = Map::find($hosp->map_id);
        return \View::make('maps/show',compact('hosp','map'));
    }

    public function show($id)
    {
        $pharmacy = Pharmacy::all();
        $tipo ='busqueda';
        $title='Busqueda de Farmacias';
        return \View::make('pharmacy/list',compact('pharmacy','tipo','title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $phar = Pharmacy::find($id);
                return \View::make('pharmacy/update',compact('phar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $phar = Pharmacy::find($request->id);
                $phar->name = $request->name;
        $phar->save();
                return redirect('pharmacy');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $phar = Pharmacy::find($id);
        $aseg=Insurance::all();

        foreach($aseg as $r){
           $phar->insurance()->detach($r->id);
        }
        
        $phar->delete();

         Session::flash('message','La eliminacion de '.$phar->name.' se realizo con exito!!');
        return redirect()->back();
        
    }

    public function search(Request $request){
         $hosp = pharmacy::where('name','like','%'.$request->name.'%')->get();
         return \View::make('pharmacy/list', compact('hosp'));
        
    }

    public function insuredsearch(Request $request){
         $hist = Insured::join('history', 'insured.id', '=', 'history.insured_id')
                                ->select('history.*', 'insured.first_name','insured.last_name','insured.identity_number','insured.policy_number')
                                // ->where('users.identity_number','=',$request->identity_number)
                                ->where('history.id','=',$request->history_number)
                                ->get();
         //dd($hist);exit;
         $doctor= User::find($hist[0]->user_id)->personal;                      
         if(count($hist)<=0){
            Session::flash('message','No se encontro la Historia medica indicada!!');

        return redirect()->back();

         } else return \View::make('pharmacy/insured_search', compact('hist','doctor'));
        
    }

    public function findinsured(Request $request){
         
         return \View::make('pharmacy/insured_search');
        
    }
}
