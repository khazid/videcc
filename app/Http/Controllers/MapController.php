<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hospital;
use App\Pharmacy;
use App\Map;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MapController extends Controller
{
     protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $map = new Map;
       $map->name = $request->name;
        $map->lat = $request->lat;
        $map->lng = $request->lng;
        $map->save();
        
        $mapsnew=$map->id; 
    
        if($request->tipo=='ho'){ 

           $hos=Hospital::find($request->id);
           
            //se elimina el mapa anterior guardado para esa farmacia
           $mapold=Map::find($hos->map_id);
           $mapold=delete();
           //-----------------------
           
           // se agrega el id del nuevo mapa guardado
           $hos->map_id=$mapsnew->id;
           $hos->save();
            
            return redirect('hospital');
         
         }else if($request->tipo=='ph'){
            
            $hos=pharmacy::find($request->id);
            
            //se elimina el mapa anterior guardado para esa farmacia
            $mapold=Map::find($hos->map_id);
            $mapold=delete();
            //-----------------------
            
            // se agrega el id del nuevo mapa guardado
            $hos->map_id=$mapsnew->id;
            $hos->save();
            
            return redirect('pharmacy');

         }   
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    public function add($id,$tipo)
    {
        if($tipo=='ho'){ 
            $hosp = Hospital::find($id);
        }else if($tipo=='ph'){
            $hosp = Pharmacy::find($id);
        }
        return \View::make('maps/map',compact('hosp','tipo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
