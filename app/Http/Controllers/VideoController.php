<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\History;
use App\BackGround;
use App\User;
use App\Roles;
use App\Insured;
use Auth;
use Mail;
use Debugbar;
use App\ClaimInsurance;
use Input;
use DB;
use App\ippbx;
use App\sipml5;
use App\Hospital;
use App\Callcenter;
use App\Calls;

class VideoController extends Controller
{
     protected $redirectTo = 'auth/login';
     
     public function __construct() {
        $this->middleware('auth');
      }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
      $id_user=Auth::user()->id;
      $ippbxdata = ippbx::where('id_user',$id_user)->get();
      
              
      $sipdata = sipml5::all();
      $sata = $sipdata[0];
      // dd($sata);exit;
      $ippbxdata = $ippbxdata[0];
      $logcall='false';
      return \View::make('video/video',compact('id_user','ippbxdata','sata','hospital','logcall'));
       
    }

   

    public function buscaraseg(Request $request)
    {   
       if($request->ajax()) {
        $id_user=Auth::user()->id;
        $id=$request->id;
        $aseg= User::where('id',$id)->get();
                             
        
        // ------ GUARDAR DATOS DE LA LLAMADA 
            $callcenter = new Callcenter;
            $callcenter->id_user=$id_user;
            $callcenter->insured_id=$aseg[0]['id'];
            $callcenter->created_at=date('Y-m-d H:i:s');
            $callcenter->save();

        return Response::json(['aseg'=>$aseg,'id_user'=>$id_user,'callcenter'=>$callcenter->id]);
        
       }
       

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  
      
      $id_user=Auth::user()->id;
      $ippbxdata = ippbx::where('id_user',$id_user)->get();
      $hospital=Hospital::lists('name','name');
      $sipdata = sipml5::all();
      $sata = $sipdata[0];
      // dd($sata);exit;
      $ippbxdata = $ippbxdata[0];
      $idinsured=$request->insuredid;
      $logcall=$request->callcenter;



                  // guardar historia en llamada telefonica/ videollamada
                  if($request->callcenter=='true' && $request->busy=='false'){ //conectado a central y atiende llamada telefonica
                     $callcenter=Callcenter::find($request->callcenterid);
                     $callcenter->history_id=$history->id;
                     $callcenter->ended_at=date('Y-m-d H:i:s');
                     $callcenter->caller_file=$request->callerfile;
                     $callcenter->called_file=$request->calledfile;
                     $callcenter->save();
                   
                  }elseif(($request->callcenter=='true' && $request->busy=='true') || ($request->callcenter=='false' && $request->busy=='true') ){ //conectado a central pero atiende videollamada o no conectado a central y atiende viedeollamada
                     $video=Calls::find($request->videocallid);
                     $video->history_id=$history->id;
                     $video->caller_file=$request->callerfile;
                     $video->called_file=$request->calledfile;
                     $video->save();
                     //$logcall="false";
                  }
                  

                  //envio de historia 
                  if($request->envio){
                   
                    $aseguser= Insured::find($idinsured);
                    $aseg= Insured::find($idinsured)->user;
                    $doctor= User::find($id_user)->personal; 
                    $hist= History::find($history->id);   
                    //dd('aseg'.$aseg);exit;
                   
                    //generar pdf con la historia
                    //return \View::make('medichistory/history',compact('aseguser','doctor','aseg','hist'));


                    $name= 'historia_'.$aseg->name.'_'.$hist->id.'_'.date('d-m-Y').'.pdf';
                    $path= public_path().'/pdf/'.$name;
                    $pdf = \PDF::loadView('medichistory/history_split',compact('aseguser','doctor','aseg','hist'));
                    $pdf->save($path);
                   
                   $this->sendEmail($idinsured,$path);
                     
                     \Session::flash('message','Envio de Historia Medica de "'.$aseg->name.'" se realizo con exito!!');

                  }


      return \View::make('video/video',compact('id_user','ippbxdata','sata','hospital','logcall'));


    }

    public function sendEmail($id,$path)
    {
        $insured = Insured::find($id)->user;
        Mail::send('emails.history', ['user' => $insured], function ($m) use ($insured,$path) {
            $m->from('consultas@doctornow24.com', 'Historia Medica online');

            $m->to($insured->email, $insured->name)->cc($insured->email2, '')->subject('Tu Historia Medica Drnow24!');

            $m->attach($path);
           
        });

        \File::Delete($path);
    }

    
}
