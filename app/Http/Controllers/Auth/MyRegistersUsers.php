<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Role_user as Role_user;
use App\User as User;
use App\Personal as Personal;
use App\Insured as Insured;

trait MyRegistersUsers
{
    public function getRegisterAdm()
    {
        return view('auth.register_adm');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

       // Auth::login($this->create($request->all())); -->Inicio de Sesion

       //Creacion de Usuario 
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->active = 'TRUE';
        $user->identity_number = $request->cedula;
        $user->password = bcrypt($request->password);
        if($user->save()){
            //Asignacion de Rol
            $rol = new Role_user;
            $rol->role_id = 3;  // Rol de Asegurado
            $rol->user_id = $user->id;
            $rol->save();            
        }      
        
        return redirect($this->redirectPath());
    }

    public function postRegisterAdm(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

       // Auth::login($this->create($request->all())); -->Inicio de Sesion
        
        
       //Creacion de Usuario 
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->active = 'TRUE';
        $user->identity_number = $request->cedula;
        $user->password = bcrypt($request->password);
        if($user->save()){
            //Asignacion de Rol
            $rol = new Role_user;

            switch ($valor = $request->slug) {       // Rol de Asegurado
                case 'admin':                                       
                    $rol->role_id = 1;
                break;
                case 'staff':                                             
                    $rol->role_id = 2;
                break;
                case 'insured':                                             
                    $rol->role_id = 3;
                break;
                case 'insurer':                                             
                    $rol->role_id = 4;
                break;
                case 'doctor':                                             
                    $rol->role_id = 5;
                break;
                case 'pharmacist':                                             
                    $rol->role_id = 6;
                break;              
            }
            $rol->user_id = $user->id;
            $rol->save(); 

             $personal = Personal::find($request->actived);
             $personal->active = 'TRUE';  
             $personal->save();          
        }      
        
        return redirect($this->redirectPath());
    }
}