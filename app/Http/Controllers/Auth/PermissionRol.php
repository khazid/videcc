<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Role_user as Role_user;
use App\User as User;
use App\Personal as Personal;
use App\Insured as Insured;
use App\MenuRol;
use App\PermRol;

trait PermissionRol
{
     public function options()
    {
       $mrol= MenuRol::join('menu', 'menu.id', '=', 'menu_role.menu_id')
                                ->join('role_user', 'role_user.role_id', '=', 'menu_role.role_id')
                                ->select('role_user.role_id as idrol', 'menu.name as menuname', 'menu.id as idmenu','menu_role.active','menu_role.id')
                                ->where('role_user.user_id','=',$this->user()->id)
                                ->where('menu_role.active','=',TRUE)
                                ->orderby('position')
                                ->get();
                
        
        return $mrol;
    }

    /**
     * Find the permissions of the rol.
     **/
    public function permission()
    {
       $mrol= PermRol::join('permissions', 'permissions.id', '=', 'permission_role.permission_id')
                                ->join('role_user', 'role_user.role_id', '=', 'permission_role.role_id')
                                ->select('role_user.id as idrol', 'permissions.name as permname', 'permissions.id as idperm','permission_role.active','permission_role.id','permissions.slug','permissions.menu_id')
                                ->where('role_user.user_id','=',$this->user()->id)
                                ->where('permission_role.active','=',TRUE)
                                ->get();
                
        
        return $mrol;
    }
}