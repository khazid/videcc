<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class RolController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Assign Rol Controller
    |--------------------------------------------------------------------------
    |
    */

    use App\User;
    use Bican\Roles\Models\Role;

    protected $redirectTo = 'auth/login';

    /**
     * Assign a Rol to a user.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function assign($id){
        $id = DB::table('users')->where('name', 'John')->value('email');
        $user = User::find($id);
        $user->attachRole($Role);
    }

    public function assignrol(){

        $role = Role::find($roleId);
        $role->attachPermission($createUsersPermission); 
        // Asignamos un permiso a determinado Role

        $user = User::find($userId);
        $user->attachPermission($deleteUsersPermission); 
        // asignamos un permiso a un usuario

        $role->detachPermission($createUsersPermission); 
        // Quitamos un permiso determinado a un role
        $role->detachAllPermissions(); 
        // Quitamos todos los permisos a un role

        $user->detachPermission($deleteUsersPermission);
        // Quitamos un permiso determinado a un usuario
        $user->detachAllPermissions();
        // Quitamos todos los permisos a un usuario
    }

    public function assignpermision(){

        $role = Role::find($roleId);
        $role->attachPermission($createUsersPermission); // permission attached to a role

        $user = User::find($userId);
        $user->attachPermission($deleteUsersPermission); // permission attached to a user

        $role->detachPermission($createUsersPermission); // in case you want to detach permission
        $role->detachAllPermissions(); // in case you want to detach all permissions

        $user->detachPermission($deleteUsersPermission);
        $user->detachAllPermissions();
    }
}
