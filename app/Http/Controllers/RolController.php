<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Roles as Roles;
use App\MenuRol as MenuRol;
use App\Menu as Menu;
use App\Permissions as Perm;
use App\PermRol as Permrol;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RolController extends Controller
{
    protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Roles::all(); 
        return \View::make('menu/listrol',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return \View::make('menu/newrol');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rol = new Roles;
        $rol->create($request->all());
        
        //asignacion de todas las opciones del menu
          $rols=Roles::all();
          $rolsnew=Roles::find($rols->last()->id);        
          $menu=Menu::all();
       
        foreach($menu as $r){
           $rolsnew->menus()->attach($r->id,['active'=>false,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d')]);
        }
        //----------------------------------------

        //asignacion de todos los permisos del sistema

        $permisos=Perm::all();
       
        foreach($permisos as $p){
           $rolsnew->perms()->attach($p->id,['active'=>false,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d')]);
        }
        //--------------------------------------------
        
        //******************************************
        // Al crear un nuevo rol se le asignan todos 
        //los permisos y opciones y desde el administrador 
        //de rol se le activan las necesarias
        //******************************************
        
        return redirect('rol');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
                $rol = Roles::find($id);
                
                $mrol= MenuRol::join('menu', 'menu.id', '=', 'menu_role.menu_id')
                                ->join('roles', 'roles.id', '=', 'menu_role.role_id')
                                ->select('menu_role.role_id', 'menu.name as menuname', 'menu_role.menu_id','menu_role.active','menu_role.id')
                                ->where('roles.id','=',$id)
                                ->get();
                
                $perm = Permrol::join('permissions', 'permissions.id', '=', 'permission_role.permission_id')
                                ->join('roles', 'roles.id', '=', 'permission_role.role_id')
                                ->select('permission_role.role_id', 'permissions.name', 'permission_role.permission_id','permission_role.active','permission_role.id')
                                ->where('roles.id','=',$id)
                                ->get();
                return \View::make('menu/updaterol',compact('rol','option','perm','mrol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rol = Roles::find($request->id);
                $rol->name = $request->name;
                $rol->slug = $request->slug;
                $rol->description = $request->description;
        $rol->save();

                return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $rol = Roles::find($id);
        $rol->delete();

        $menu=Menu::all();
       
        foreach($menu as $r){
           $rol->menus()->detach($r->id);
        }
         $permisos=Perm::all();
       
        foreach($permisos as $p){
           $rol->perms()->detach($p->id);
        }

        Session::flash('message','La eliminacion de '.$rol->name.' se realizo con exito!!');
        return redirect()->back();
    }

    public function search(Request $request){
         $roles = Roles::where('name','like','%'.$request->name.'%')->get();
         return \View::make('menu/listrol', compact('roles'));
        
    }
}
