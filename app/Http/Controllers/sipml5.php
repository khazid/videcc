<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class sipml5 extends Controller
{
   public function index(){
$ippbxdata = DB::select('select * from ippbx_connection');
$sipdata = DB::select('select * from sipml5_call_settings');
return view('drsipml5/call',['ippbxdata'=>$ippbxdata],['sipdata'=>$sipdata]);
}
}
