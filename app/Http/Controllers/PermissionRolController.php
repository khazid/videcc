<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PermRol as Permrol;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;

class PermissionRolController extends Controller
{
    protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prol = new PermRol;
        $prol->create($request->all());
        
        return redirect('menurol');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {   $id=Input::get('id');
        $checkbox = Input::get('permiso');
        
         foreach($checkbox as $c){
          
            if(substr($c, 0,1)  == 'p'){ 
                $c=substr($c,1);
                $valor=FALSE;
            }else {
                 $valor=TRUE;
            }
            
            $mrol = Permrol::find($c);
            $mrol->active =$valor ;
            $mrol->save();

         }
       return back()->withInput();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
