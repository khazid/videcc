<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Permissions as Perm;
use App\Menu as Menu;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Roles as Roles;

class PermissionsController extends Controller
{
    protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        $perm= Perm::join('menu', 'menu.id', '=', 'permissions.menu_id')
                                ->select('permissions.name', 'permissions.id','permissions.slug','permissions.description','permissions.menu_id','menu.name as menuname')
                                ->get();
        $option= Menu::lists('name','id');
                           
        return \View::make('menu/listperm',compact('perm','option'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return \View::make('menu/newperm');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $perm = new Perm;
        $perm->create($request->all());
        
        //asignacion del permiso a los roles 
          $perms=Perm::all();
          $permsnew=Perm::find($perms->last()->id);        
          $rol=Roles::all();
       
        foreach($rol as $r){
           $permsnew->rolesp()->attach($r->id,['active'=>false,'created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d')]);
        }
        //----------------------------------------
        
        //******************************************
        // Al crear un nuevo permiso se le asignan a  
        //todos los roles y desde el administrador 
        //de rol se le activan las necesarias
        //******************************************
        return redirect('perm');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
                
                $perm=Perm::join('menu', 'menu.id', '=', 'permissions.menu_id')
                                ->select('permissions.name', 'permissions.id','permissions.slug','permissions.description','permissions.menu_id','menu.name as menuname')
                                ->where('permissions.id','=',$id)
                                ->get();
                $option= Menu::lists('name','id');
                return \View::make('menu/updateperm',compact('perm','option'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $perm = Perm::find($request->id);
                $perm->name = $request->name;
                $perm->slug = $request->slug;
                $perm->description = $request->description;
                $perm->menu_id = $request->menu_id;
        $perm->save();
                return redirect('perm');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $perm = Perm::find($id);
        $perm->delete();
        $rol=Roles::all();

        foreach($rol as $r){
           $perm->rolesp()->detach($r->id);
        }

        Session::flash('message','La eliminacion del Permiso'.$perm->name.' se realizo con exito!!');
        return redirect()->back();
    }

    public function search(Request $request){
         $perm = Perm::where('name','like','%'.$request->name.'%')->get();
         return \View::make('menu/listperm', compact('perm'));
        
    }

}
