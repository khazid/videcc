<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Response;
use App\Personal;
use App\Menu;
use App\Insured;
use App\ClaimInsurance;
use App\BackGround;
use App\History;


class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $days = Input::get('days', 7);

        $range = \Carbon\Carbon::now()->subDays($days);

        $data = Personal::select('slug',\DB::raw('count(id) as cantidad'))->groupBy('slug')->get();
            /*echo "<pre>";
            print_r($data);
            echo "</pre>";
            exit;*/
        foreach($data as $dat)
          {
              
              $donut[] = (object)array("label" => $dat->slug, "value" => $dat->cantidad);
          }
        //$this->layout->content = View::make('admin.stats.list', compact('stats'));

        
        return \View::make('reports/charts', compact('data','donut'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $start=date('Y').'-07-01';
        $end=date('Y').'-'.date('m').'-30';
        
        $assigned= Insured::join('users','users.identity_number','=','insured.identity_number')
                        ->join('history','history.insured_id','=','insured.id')
                        ->join('insurance_claim','insurance_claim.history_id','=','history.id')
                        ->join('policy_insured','policy_insured.identity_number','=','insured.identity_number')
                        ->select('insurance_claim.id','insurance_claim.claim_number','insurance_claim.approved','policy_insured.policy_number','insured.id as insuredid','insured.first_name','insured.last_name','history.reason','history.id as historyid','history.current_illness','history.treatment','history.created_at','users.background_id')
                        ->whereBetween('history.created_at',[$start,$end])
                        ->orderBy('history.created_at')
                        ->get();

       $title[0]='Fecha Siniestro';
       $title[1]='Numero';
       $title[2]='poliza';
       // $title[3]='Pariente';
       $title[3]='Sintomas'; 
       $title[4]='Diagnostico'; 
       // $title[6]='Area Medica';
       // $title[5]='Indicaciones'; 
       $title[5]='Antecedentes'; 
       $title[6]='Aprobacion';  

                   
       return \View::make('reports/list', compact('assigned','title'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //--------------------------------------------------------------------------------------------------
public function listCharts(Request $request)
    {   
       

        $start=$request->start;
        $end=$request->end;
       
        
        if($request->lista=='siniestros'){ 
            $this->siniestros($start,$end);
        }


    }

public function siniestros(Request $request)
    {   
       
        $start=$request->start;
        $end=$request->end;
        
        
        $assigned= Insured::join('users','users.identity_number','=','insured.identity_number')
                        ->join('history','history.insured_id','=','insured.id')
                        ->join('insurance_claim','insurance_claim.history_id','=','history.id')
                        ->join('policy_insured','policy_insured.identity_number','=','insured.identity_number')
                        ->select('insurance_claim.id','insurance_claim.claim_number','insurance_claim.approved','policy_insured.policy_number','insured.id as insuredid','insured.first_name','insured.last_name','history.reason','history.id as historyid','history.current_illness','history.treatment','history.created_at','users.background_id')
                        ->whereBetween('history.created_at',[$start,$end])
                        ->orderBy('history.created_at')
                        ->get();

       $title[0]='Fecha Siniestro';
       $title[1]='Numero';
       $title[2]='poliza';
       // $title[3]='Pariente';
       $title[3]='Sintomas'; 
       $title[4]='Diagnostico'; 
       // $title[6]='Area Medica';
       $title[5]='Indicaciones'; 
       $title[6]='Antecedentes'; 
       $title[7]='Aprobacion';    

                   
       return \View::make('reports/list', compact('assigned','title'));
    

    }

  public function showcalls(){

       return \View::make('reports/call_list'); 
  }

  public function calls(Request $request)
    {   
       
        $start=$request->start;
        $end=$request->end;
        
      if($request->type=='video'){  

              $calls= Insured::join('calls','calls.caller_id','=','insured.id')
                              ->join('users','users.id','=','calls.recipient_id')
                              ->join('personal','users.identity_number','=','personal.identity_number')
                              ->select('calls.id','calls.created_at','calls.ended_at','calls.caller_file','calls.called_file','calls.history_id','insured.id as insuredid','insured.first_name','insured.last_name','personal.first_name as docname','personal.id as personalid','personal.last_name as doclastname',\DB::raw('(SUM(CAST( calls.ended_at AS TIME ))) - (SUM(CAST( calls.created_at AS TIME )))  as total'))
                              ->whereBetween('calls.created_at',[$start,$end])
                              ->groupBy("calls.id","insured.id","personal.id")
                              ->orderBy('calls.created_at')
                              ->get();

      }else if($request->type=='call'){
            $calls= Insured::join('callcenter','callcenter.insured_id','=','insured.id')
                              ->join('users','users.id','=','callcenter.id_user')
                              ->join('personal','users.identity_number','=','personal.identity_number')
                              ->select('callcenter.id','callcenter.created_at','callcenter.ended_at','callcenter.caller_file','callcenter.called_file','callcenter.history_id','insured.id as insuredid','insured.first_name','insured.last_name','personal.id as personalid','personal.first_name as docname','personal.last_name as doclastname',\DB::raw('(SUM(CAST( callcenter.ended_at AS TIME ))) - (SUM(CAST( callcenter.created_at AS TIME )))  as total'))
                              ->whereBetween('callcenter.created_at',[$start,$end])
                              ->groupBy("callcenter.id","insured.id","personal.id")
                              ->orderBy('callcenter.created_at')
                              ->get();

      }
       $title[0]='id ';
       $title[1]='Doctor';
       $title[2]='Asegurado';
       $title[3]='Inicio'; 
       $title[4]='Finalizacion'; 
       $title[5]='Tiempo total'; 
       $title[6]='Nro Historia';
       $title[7]='Archivo'; 
         


                   
       return \View::make('reports/call_list',compact('title','calls')); 
    

    }

    public function approveclaim(Request $request)
    {
      if($request->ajax()) {
        
        $claim = ClaimInsurance::find($request->id);

        $claim->approved= $request->valor;
        $claim->save();                 
       
       return Response::json(['approve'=>$claim]);
        
       }
    
    }

    public function background(Request $request)
    {
      if($request->ajax()) {
        
       $back = BackGround::where('insured_id',$request->id)->get();
       $cont=count($back);           
       if(count($back)>0) {
        

               return Response::json(['background'=>$back,'cont'=>$cont ]);
              }else return Response::json(['background'=>'404','cont'=>$cont ]);

        
       }
    
    }

    public function history(Request $request)
    {
      if($request->ajax()) {
        
       $back = History::where('id',$request->id)->get();
       $cont=count($back);           
       if(count($back)>0) {
        

               return Response::json(['background'=>$back,'cont'=>$cont ]);
              }else return Response::json(['background'=>'404','cont'=>$cont ]);

        
       }
    
    }

}
