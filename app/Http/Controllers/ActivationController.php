<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ActivationRequest;
use App\Http\Requests\ActivationInsuredRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Controllers\Controller;
use Auth;
use App\Personal;
use App\User;
use App\RoleUser;
use App\Insured;
use App\Insurance;
use App\Countries;
use App\Roles;
use App\ippbx;
use App\sipml5;
use Mail;

class ActivationController extends Controller
{
    
    
    public function getFind()
    {
    	return \View::make('activation/find');
    }


    //Metodos para Asegurados //

    public function getFindInsured()
    {
    	$data=Roles::whereIn('id',[2,3])->orderby('name')->lists('name','id');
        $data->prepend('Seleccione', '');
        return \View::make('activation/findInsured',compact('data'));
    }

    public function getFindInsuredMobile()
    {
        $data=Roles::whereIn('id',[2,3])->orderby('name')->lists('name','id');
        $data->prepend('Seleccione', '');
        return \View::make('activation/mobile',compact('data'));
    }

    
    public function postValidateInsured(Request $request)
    {   
        //CAPTCHA
        


        $access = $request->accestype;
        $name = $request->name;
        $email = $request->email;
        // dd( $request->insurance_id); exit;
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'accestype' => 'required',
            ]);

        $info = User::join('role_user','users.id','=','role_user.user_id')
                        ->join('roles','roles.id','=','role_user.role_id')
                        ->select('roles.name as role','users.email','users.name')
                        ->where('email',$request->email)->get();
       
         //dd('Request: ',$request);exit;               
        if (count($info)<=0) { //si la persona no esta registrada 
         
            $user = new User;
                    $user->name = $request->name;
                    $user->email = $request->email;
                    $user->active = TRUE;
                    $user->save();
                
           
                $roleuser= new RoleUser;
                    $roleuser->role_id = $request->accestype;
                    $roleuser->user_id = $user->id;
                    $roleuser->save();
            

        }
        
           $info = User::where('email',$request->email)->get();  
        //dd('Informacion del usuario: ',$info);exit;
        
            if($request->accestype==2){
                        
                        if($request->mobile){
                            return \View::make('activation/video_mobile',compact('info')); 
                        }else {              
                            return \View::make('activation/video',compact('info')); 
                        }        
            
            }else if($request->accestype==3){
                  $id_user=$info[0]->id;
                  $ippbxdata = ippbx::where('id_user',2)->get();
                  
                          
                  $sipdata = sipml5::all();
                  $sata = $sipdata[0];
                  // dd($sata);exit;
                  $ippbxdata = $ippbxdata[0];
                  $logcall='false';

                 return \View::make('video/video',compact('id_user','ippbxdata','sata','logcall'));
                    

            }
    }


     //Test de Video y Audio//

    public function getView()
    {
        return \View::make('activation/video_test');
    }

    

}