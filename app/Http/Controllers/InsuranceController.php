<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Insurance;
use App\Countries;

class InsuranceController extends Controller
{
     protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $insur = Insurance::join('countries', 'countries.id', '=', 'insurance.countries_id')
                                ->select('insurance.name', 'insurance.id','countries.id as idcountrie','countries.name as namecountrie')
                                ->get();
        $countrie= Countries::orderBy('name')->lists('name','id'); 
        return \View::make('insurance/list',compact('insur','countrie'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countrie= Countries::orderBy('name')->lists('name','id');
        return \View::make('insurance/new',compact('countrie'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $insur = new Insurance;
        $insur->name=$request->name;
        $insur->countries_id=$request->countries_id;
        $insur->save();
        return redirect('insurance');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $insur = Insurance::join('countries', 'countries.id', '=', 'insurance.countries_id')
                                ->select('insurance.name as name', 'insurance.id as id','countries.id as idcountrie','countries.name as namecountrie')
                                ->where('insurance.id','=',$id)
                                ->get();
         // echo "<pre>";
         // print_r($insur);
         // echo "<pre>";exit;

         $countrie= Countries::orderBy('name')->lists('name','id');
         return \View::make('insurance/update',compact('insur','countrie'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $insur = Insurance::find($request->id);
                $insur->name = $request->name;
                $insur->countries_id = $request->countries_id;
        $insur->save();
                return redirect('insurance');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $insur = Insurance::find($id);
        $insur->delete();

        Session::flash('message','La eliminacion de "'.$insur->name.'" se realizo con exito!!');
        return redirect()->back();
    }
    public function search(Request $request){
         $insur = Insurance::where('name','like','%'.$request->name.'%')->get();
         return \View::make('insurance/list', compact('insur'));
        
    }
}
