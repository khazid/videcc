<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Personal as Personal;
use App\Role_user as Role_user;
use App\Auth\AuthController as AuthController;
use App\Insured as Insured;
use App\Roles;
use Input;
use URL;
use Auth;
use App\User;

class PersonalController extends Controller
{
      protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personals = Personal::all();
        return \View::make('staff.list',compact('personals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $option= Roles::lists('name','slug');
        return \View::make('staff.new',compact('option'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $personal = new Personal;
        $personal->first_name = $request->name;
        $personal->active = true;
        $personal->last_name = $request->last_name;
        $personal->identity_number = $request->identity_number;
        $personal->slug = $request->slug;
        $personal->specialties = $request->specialties;
        $personal->access_code = bcrypt($request->name.$request->slug.$request->last_name);
        $personal->save();
       /*if($personal->save()){
            //Se genera el Rol por el Tipo de Usuario
        $rol = new Role_user;
        $rol->role_id =2;
        $rol->user_id =17;
        $rol->save();
       }     */   
        return redirect('personal');
    }

  

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $per = Personal::find($id);
        $file= url('images/personal').'/'.$id.'.jpg';

        /*echo "<pre>";
        print_r($per);
        echo "</pre>";*/
                return \View::make('staff.show',compact('per','file'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pers = Personal::find($id);
            return \View::make('staff.update',compact('pers'));
    }

    /**
     * Busco Si existe el usuario en la Tabla personal para traer los datos.
     *
     * @param  int  $id -->Cedula
     * @return \Illuminate\Http\Response
     */
    public function getRegisterStaff()
    {
        return view('staff.register_adm');
    }

    public function registerStaff(Request $request)
    {
         $person = Personal::where('identity_number','=', $request->name)
            ->where('active','=','FALSE')
            ->first();

        if ($person)
        {
            return redirect()->route('auth/registerStaff', ['id' => $person->id]);
        }

        return back()->withInput();        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $personal = Personal::find($request->id);
                $personal->first_name = $request->first_name;
                $personal->last_name = $request->last_name;
                $personal->identity_number = $request->identity_number;
                $personal->slug = $request->slug;                
        if($personal->save()){
           
            if($request->hasFile('image')) {
                $imageName = $request->id . '.' .$request->file('image')->getClientOriginalExtension();

                $request->file('image')->move(base_path() . '/public/images/personal/', $imageName);
                 
            }

            return redirect('personal');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $personal = Personal::find($id);
        $personal->delete();

        $user=User::where('identity_number','=',$personal->identity_number);
        $user->delete();

        Session::flash('message','La eliminacion de '.$personal->first_name.' '.$personal->last_name.' se realizo con exito!!');
        return redirect()->back();
    }

    public function search(Request $request)
    {
         $personals = Personal::where('first_name','like','%'.$request->name.'%')
                                ->orWhere('last_name','like','%'.$request->name.'%')
                                ->orWhere('identity_number','like','%'.$request->name.'%')
                                ->get();
         return \View::make('staff.list', compact('personals'));
        
    }

        /**
     * Busco Si existe el usuario en la Tabla Insured para traer los datos.
     *
     * @param  policy_number
     * @return \Illuminate\Http\Response
     */
    public function getRegisterInsured()
    {
        return view('auth.register_insured');
    }

    public function registerInsured(Request $request)
    {
         $insured = Insured::where('policy_number','=', $request->name)
                        ->where('use_policy_number','=','FALSE')
                             ->get();
        
        return \View::make('auth.register',compact('insured'));
       
        
    }

    public function reset()
    {
        $id=Auth::user()->id;

         $user = User::where('id','=',$id)
                             ->get();
        
        return \View::make('auth.reset',compact('user'));
       
        
    }

    public function updatepass(Request $request)
    {   $id=Auth::user()->id;
        $user=User::find($id);
        $user->password = bcrypt($request->password);
        $user->save();
        return \View::make('home');
       
        
    }

    public function activestaff($id)
    {   $personal = Personal::withTrashed()->where('id','=',$id )->restore();
        $personal = Personal::find($id);
        $user=User::withTrashed()->where('identity_number','=',$personal->identity_number)->restore();
        
        
        Session::flash('message','La activacion de '.$personal->first_name.' '.$personal->last_name.' se realizo con exito!!');
        return redirect()->back();
       
        
    }

    public function searchdeleted()
    {    $deleted = Personal::onlyTrashed()->get();
        //dd($deleted);exit;
        return \View::make('staff.activeStaff',compact('deleted'));
       
        
    }


}
