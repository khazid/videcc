<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Insured;
use App\History;


class PdfController extends Controller
{
   public function github (){
    $idinsured=1;
    $id_user=4;
    $history=104;
    
    $aseguser= Insured::find($idinsured);
    $aseg= Insured::find($idinsured)->user;
    $doctor= User::find($id_user)->personal; 
    $hist= History::find($history); 
    
    return \PDF::loadView('medichistory/pdftest',compact('aseguser','doctor','aseg','hist'))->stream();

 	}

 	 public function downloadpdf ($id,$hist,$doctor){
 			$ins= User::join('history', 'history.insured_id', '=', 'users.id')
                                ->join('insured', 'insured.identity_number', '=', 'users.identity_number')
                                ->select('history.*', 'insured.first_name','insured.last_name','insured.identity_number','insured.policy_number')
                                ->where('users.id','=',$id)
                                ->where('history.id','=',$hist)
                                ->get();	
            
      $doctor= User::join('personal', 'personal.identity_number', '=', 'users.identity_number')
                                ->select('personal.*')
                                ->where('users.id','=',$doctor)
                    			->get();	
            //generar pdf con la historia
           // $name= 'historia_'.$aseg->name.'_'.$hist.'_'.$aseg->created_at.'.pdf';
             //$name= 'historia.pdf';
                			
     return \PDF::loadView('medichistory/history', compact('ins', 'doctor'))->stream('historia.pdf');
 	}
}
