<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Hospital;
use App\Map;
use App\Insurance;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;


class HospitalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $redirectTo = 'auth/login';
     public function __construct() {
        $this->middleware('auth');
    }
    public function index()
    {
        $hospital = Hospital::all();
        $tipo='config';
        $title='Administracion de Hospitales y Clinicas';

        $asegura= Insurance::lists('name','id');
        
        return \View::make('hospital/list',compact('hospital','tipo','title','asegura'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $asegura= Insurance::all();
        $tipo='ho';
        return \View::make('hospital.new',compact('asegura','tipo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        //para guardar la info del hospital en la tabla
       $hospital = new Hospital;
       $hospital->name = $request->name;
       $hospital->address = $request->addressh;
       $hospital->map_id = 0;
       $hospital->save();
      
       //se busca el id que se acaba de insertar
       $hid= $hospital->id;
       
        //se guarda la informacion del mapa
       $map = new Map;
        $map->name = $request->name;
        $map->lat = $request->lat;
        $map->lng = $request->lng;
        $map->save();

       // se busca el ultimo id del mapa guardado
       $maps=$map->id;
        
        // se actualiza la tabla de hospital con el id del mapa
           $hospital->map_id=$maps;
           $hospital->save();
            
        //asignacion del hospital a la aseguradoras correspondientes

          $checkbox = Input::get('insurance');
            
             foreach($checkbox as $c){
                 
               if(substr($c, 0,1)  != 'i'){ 
                    $idi=substr($c,0,1);
                    
                    $hospital->insurance()->attach($idi,['created_at' => date('Y-m-d'),'updated_at' => date('Y-m-d')]);
                }
                
             }
            
        //----------------------------------------
        return redirect('hospital');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function map($id)
    {
        $hosp = Hospital::find($id);
        $map = Map::find($hosp->map_id);
        return \View::make('maps/show',compact('hosp','map'));
    }

    public function show($id)
    {
        $hospital = Hospital::all();
        $tipo ='busqueda';
         $title='Busqueda de Hospitales y Clinicas';
        return \View::make('hospital/list',compact('hospital','tipo','title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $hosp = Hospital::find($id);
         $asegura= Insurance::all();
                return \View::make('hospital/update',compact('hosp','asegura'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hosp = Hospital::find($request->id);
                $hosp->name = $request->name;
        $hosp->save();
                return redirect('Hospital');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $hosp = Hospital::find($id);
        
        $aseg=Insurance::all();

        foreach($aseg as $r){
           $hosp->insurance()->detach($r->id);
        }
        $hosp->delete();

        Session::flash('message','La eliminacion de "'.$hosp->name.'" se realizo con exito!!');
        return redirect()->back();
        
    }

     public function search(Request $request){
         $hosp = Hospital::where('name','like','%'.$request->name.'%')->get();
         return \View::make('Hospital/list', compact('hosp'));
        
    }

}
