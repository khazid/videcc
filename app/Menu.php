<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
   	use SoftDeletes;
    protected $table = 'menu';
  	protected $fillable = ['name','position'];
  	protected $guarded = ['id']; 
    protected $dates = ['deleted_at'];

  	public function roles()
    {
        return $this->belongsToMany('App\Roles', 'menu_role','menu_id','role_id');
    }

    public function permisos()
    {
        return $this->hasOne('App\Permissions');
    }
}
