<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Map extends Model
{
    use SoftDeletes;
    protected $table = 'map';
  	protected $fillable = ['name','lat','lng'];
  	protected $guarded = ['id'];
  	 protected $dates = ['deleted_at'];

  	
}
