<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permissions extends Model
{
     use SoftDeletes;
    protected $table = 'permissions';
  	protected $guarded = ['id'];
  	protected $fillable = ['name','slug','description','menu_id'];
    protected $dates = ['deleted_at'];

  	public function rolesp()
    {
        return $this->belongsToMany('App\Roles', 'permission_role','permission_id','role_id');
    }

   public function menu()
    {
        return $this->belongsTo('App\Menu');
    }
   
}
