<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuRol extends Model
{
    use SoftDeletes;
    protected $table = 'menu_role';
  	protected $guarded = ['id'];
  	protected $dates = ['deleted_at'];
  	
}
