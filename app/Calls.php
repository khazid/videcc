<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Calls extends Model
{
    use SoftDeletes;

    
	protected $table = 'calls';
  	protected $fillable = ['ended_at','caller_file','called_file','history_id','caller_identity_number','caller_name','status'];
  	protected $guarded = ['id','caller_id','recipient_id'];
  	protected $dates = ['deleted_at'];
}
