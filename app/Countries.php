<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Countries extends Model
{
	use SoftDeletes;
	protected $table = 'countries';
  	protected $fillable = ['iso','name'];
  	protected $guarded = ['id'];
  	protected $dates = ['deleted_at'];

    public function insurance()
    {
        return $this->hasOne('App\Insurance');
    }
}
