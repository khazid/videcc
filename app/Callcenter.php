<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Callcenter extends Model
{
   use SoftDeletes;

    
	protected $table = 'callcenter';
  	protected $fillable = ['ended_at','caller_file','called_file','history_id'];
  	protected $guarded = ['id','id_user','insured_id'];
  	protected $dates = ['deleted_at'];
}
