<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Roles extends Model
{
    use SoftDeletes;
    protected $table = 'roles';
  	protected $guarded = ['id'];
  	protected $fillable = ['name','slug','description'];
    protected $dates = ['deleted_at'];


  	//Funcion para devolver Todos los Roles
  	public static function getListRoles()
	{
	    $return = array('2' => 'Seleccione');
	    $tramite = self::all();

	    foreach ($tramite as $trami)
	    {
	        $return[$trami->id] = $trami->name;
	        //$return['Interno'][$dep->id] = $dep->nombre;
	    }

	    return $return;

	}

	public function menus()
    {
        return $this->belongsToMany('App\Menu', 'menu_role','role_id','menu_id');
    }

    public function perms()
    {
        return $this->belongsToMany('App\Permissions', 'permission_role','role_id','permission_id');
    }
}
